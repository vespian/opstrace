# Architecture Overview

![Architecture.drawio__1_](../assets/Architecture.drawio__1_.png)

The main two components that are responsible for all provisioning, upgrades and lifecycle management of Tenants (which map to GitLab namespaces) are:

* Scheduler Operator
* Tenant Operator

Both kubernetes operators are written in Go using Kubebuilder.
The scheduler operator manages the lifecycle of all the major GitLab Observability components, while the Tenant operator manages the lifecycle of resources within a single GitLab root namespace (groups, dashboards, datasources, read/write APIs, integrations, etc).
The scheduler operator deploys a tenant operator in each tenant it manages.

The scheduler operator is responsible for managing the wider cluster resources to ensure we do not attempt to over utilize the cluster.
In other words, it will make sure that we don't provision more tenants than we should based on the available resources within the kubernetes cluster.

As we work toward sharding in the Observability SaaS (the ability to have several GitLab Observability clusters deployed in various regions), the scheduler operator will also be responsible for communicating to the sharding control plane, to communicate its available capacity and to receive assignments for tenants it should provision.

The new scheduler operator has been built with fine grain control over upgrades with the ability to checkpoint any stage in an upgrade and test for system health before proceeding to the next stage.

## Multi-Tenant Architecture Overview

![Architecture.drawio](../assets/Architecture.drawio.png)

GitLab Observability is multi-tenant, meaning it has the notion of namespaces (currently called Tenants) where namespaces are isolated from one another on top of shared infrastructure.
Data in one namespace cannot be accessed from another namespace.
Rate-limits, integrations, [Argus plugins, Argus Datasources, Argus Dashboards, etc](https://gitlab.com/gitlab-org/opstrace/opstrace-ui), are managed on a per-namespace basis in GitLab Observability.

Root level namespaces in GitLab are where account level capabilities are determined (by the namespace’s license).
GitLab Observability Tenants map to root level namespaces in GitLab such that GitLab Observability namespace limits/capabilities can also be determined by the GitLab license for the associated root namespace in GitLab.
This is represented by a Tenant Custom Resource in Kubernetes, where a tenant operator reconciles the required tenant capabilities, and the subgroup capabilities.

Each tenant in GitLab Observability has its own highly available [Argus instance](https://gitlab.com/gitlab-org/opstrace/opstrace-ui) (GitLab Observability UI).

Subgroups within the GitLab root-level namespace map to organizations in the associated Argus instance in GitLab Observability (represented by the Group Custom Resource in the diagram).
We will rename “organizations” in Argus to “groups” to align the concepts explicitly.

Inside an GitLab Observability tenant, an ingress resource will ensure the correct headers are set for the data ingestion path.
A group’s ingestion path may be: `https://opstrace.gitlab.com/api/{groupId}/metrics` and the ingress object will set the OrgId header equal to the groupId so we maintain data separation between groups.

Namespaces in GitLab Observability are only created on an as-needed basis.
When navigating to a namespace in GitLab Observability that does not yet exist, Gatekeeper checks the users access level on the namespace and if the user is an Owner, they will be presented with the option to enable observability on the namespace.

## Custom Resource Definitions

Scheduler has few accompanying CRDs.

### Cluster

This is a single Custom Resource that represents the GitLab Observability cluster.
The scheduler operator owns it.

### GitLabNamespace

These are managed by Gatekeeper.
Gatekeeper will receive a request to enable observability for a GitLab namespace, and if the correct permissions are present, Gatekeeper will create or update the GitLabNamespace Custom Resource that represents the GitLab namespace.
This keeps namespaces up to date in GitLab Observability, as they change in GitLab.

### Tenant

This Custom Resource represents a root level namespace in GitLab.
The tenant operator owns it and will provision a GitLab Observability UI (Argus) instance for each Tenant CR.
Each root level namespace in GitLab that enables observability will have its own instance of GitLab Observability UI where they can manage plugins, datasources etc in isolation.

### Group

This Custom Resource represents a namespace in the subtree of the root level namespace.
For instance, the [GitLab Observability group](https://gitlab.com/gitlab-org/opstrace) is a subgroup within the root level namespace gitlab-org.
gitlab-org will have it's own Tenant with its own instance of GitLab Observability UI and the group GitLab Observability will be managed as a group within that tenant.
A group is synonymous with a Grafana Org and a group will have it's own APIs for tracing, metrics, logs etc and users will log in to that group in the UI to visualize and manage their data.

## Finalizers

All these CRDs make use of [Kubernetes Finalizers](https://kubernetes.io/docs/concepts/overview/working-with-objects/finalizers/).
The previous GitLab Observability controller had no notion of Custom Resources and therefore could not take advantage of using finalizers.
Finalizers now ensure that during the destroy part of a custom resource lifecycle, everything is properly torn down.
This is especially important for adhering to GDPR and other regulations where users must have the ability to purge all their data from the system.
Just as users can enable Observability on a namespace, they can also disable it and purge all their data, thus freeing system resources for other tenants to be provisioned, and also adhering to regulations.

## SRE Overrides

One major lesson we learned the hard way with the previous GitLab Observability controller was that it was very difficult to override runtime specs for kubernetes resources.
The controller didn't allow for it, and any time a user would change a value in a kubernetes resource spec, the controller would immediately revert it.

With the new Operators, an override function has been built in such that overrides can be provided directly on the CR.
The operator will merge the overrides in, allowing an SRE to override any attribute of any Kubernetes resource.
Overrides on the CR make it simple to see what has been overridden.
Here's an example of the Cluster CR where we're overriding the number of replicas and the image for the gatekeeper deployment:

```yaml
apiVersion: opstrace.com/v1alpha1
kind: Cluster
metadata:
  annotations:
  creationTimestamp: "2022-05-17T19:29:57Z"
  finalizers:
  - cluster.opstrace.com/finalizer
  name: mats-dev-cluster
spec:
  dns:
    acmeEmail: ""
    dns01Challenge: {}
    externalDNSProvider: {}
  gitlab:
    groupAllowedAccess: redacted
    groupAllowedSystemAccess: redacted
    instanceUrl: redacted
    oauthClientId: redacted
    oauthClientSecret: redacted
  namespace: ""
  overrides:
    caInjector:
      components:
        deployment: {}
        ingress: {}
        service: {}
        serviceMonitor: {}
        statefulset: {}
    certManager:
      components:
        deployment: {}
        ingress: {}
        service: {}
        serviceMonitor: {}
        statefulset: {}
    clickhouse:
      components:
        deployment: {}
        service: {}
        serviceMonitor: {}
    clickhouseOperator:
      components:
        deployment: {}
        ingress: {}
        service: {}
        serviceMonitor: {}
        statefulset: {}
    externalDNS:
      components:
        deployment: {}
        ingress: {}
        service: {}
        serviceMonitor: {}
        statefulset: {}
    gatekeeper:
      components:
        deployment:
          spec:
            template:
              spec:
                replicas: 5
                containers:
                - image: opstrace/gatekeeper:761da560668a6258f8f6352395b7840078f2dc23
                  name: gatekeeper
        ingress: {}
        service: {}
        serviceMonitor: {}
        statefulset: {}
    jaegerOperator:
      components:
        deployment: {}
        ingress: {}
        service: {}
        serviceMonitor: {}
        statefulset: {}
    nginxIngress:
      components:
        deployment: {}
        ingress: {}
        service: {}
        serviceMonitor: {}
        statefulset: {}
    prometheusOperator:
      components:
        deployment: {}
        ingress: {}
        service: {}
        serviceMonitor: {}
        statefulset: {}
    redis:
      components:
        deployment: {}
        serviceMonitor: {}
    redisOperator:
      components:
        deployment: {}
        ingress: {}
        service: {}
        serviceMonitor: {}
        statefulset: {}
  target: kind
status:
  conditions:
  - lastTransitionTime: "2022-05-19T21:13:14Z"
    message: All components are in ready state
    observedGeneration: 17
    reason: Success
    status: "True"
    type: Ready
```

