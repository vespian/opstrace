# GitLab Observability Quick Start

Try GitLab Observability by cloning or forking this repo and creating a local installation.

## Step 0: Setup

### Install dependencies

Before setting up GitLab Observability components, you must have the following third-party dependencies installed and configured. These can be
done manually by yourself OR [automatically using asdf](#automatically-using-asdf)

#### Automatically using `asdf`

Installing dependencies using [`asdf`](https://asdf-vm.com/#/core-manage-asdf) lets GitLab Observability manage them for you automatically:

1. Clone this repository into your preferred location, if you haven't previously:

```shell
git clone gitlab-org/opstrace/opstrace.git
```

1. Change into the project directory:

```shell
cd opstrace
```

1. Install dependencies using `asdf`:

If you already have `asdf` installed, run:

```shell
make bootstrap
```

If you need to install `asdf` as well, run:

```shell
make install-asdf
```

Do not forget to source your profile as suggested by the command above, then run `make bootstrap` to get all dependencies.


> We currently use versions of these dependencies as pinned in the `.tool-versions` file.

In any case, make sure you have the following:

* [Kind](https://kind.sigs.k8s.io/docs/user/quick-start/#installation) for setting up a local Kubernetes cluster.
* [Docker](https://docs.docker.com/install) and [Docker Compose](https://docs.docker.com/compose/install) for sending dummy data to GitLab Observability.
* [Kubectl](https://kubernetes.io/docs/tasks/tools/#kubectl) for interacting with GitLab Observability
* [jq](https://stedolan.github.io/jq/download/) for some makefile utilities
* [Go 1.17](https://go.dev/doc/install) - 1.17 is the only supported version. [Kubebuilder doesn't support 1.18](https://github.com/kubernetes-sigs/kubebuilder/issues/2559) yet, and 1.16 will probably work but not garaunteed.

```bash
kind --version
docker --version
docker-compose --version
kubectl version
jq --version
go version
```

If running on MacOS, be sure to make sure you have enough resources dedicated to docker desktop.
We recommend:

* CPUs: 4+
* Memory: 8GB+
* Swap: 1GB+

It's possible to run with lower resources, we just know that these work.

Now we need to create a local kind cluster

```bash
make kind
```


## Step 1: Install GitLab Observability

Now deploy the scheduler:

```bash
make deploy
```

Create a GitLab Application so we can use it for authentication. In the GitLab instance you'd like to connect
GitLab Observability to, [create an OAuth Application](https://docs.gitlab.com/ee/integration/oauth_provider.html#introduction-to-oauth).
This application can be a user owned, group owned or instance-wide application. In production, we create an instance-wide
application and select "trusted" so that users are explicitly authorized without the consent screen. Here is an example of
how to configure the application. Be sure to select the API scope and to enter `http://localhost/v1/auth/callback` as the
redirect URI:

![gitlab oauth application](./assets/create-gitlab-application.png)

Create the secret holding auth data:

```bash
kubectl create secret generic \
    --from-literal=gitlab_oauth_client_id=<YOUR CLIENT ID FROM YOUR GITLAB APPLICATION> \
    --from-literal=gitlab_oauth_client_secret=<YOUR CLIENT SECRET FROM YOUR GITLAB APPLICATION> \
    --from-literal=internal_endpoint_token=<ERROR TRACKING INTERNAL ENDPOINT TOKEN> \
        dev-secret
```

Replace `<YOUR CLIENT ID FROM YOUR GITLAB APPLICATION>` and `<YOUR CLIENT SECRET FROM YOUR GITLAB APPLICATION>` with the values from your GitLab application that you just created.
Replace `<ERROR TRACKING INTERNAL ENDPOINT TOKEN>` with any string for if you do not plan to use error tracking.
You can also look at [this](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/91928) and see how you can obtain the token to test error tracking.
All the parameters above are not optional - must be set.
Next step is to create the cluster definition:

```bash
cat <<EOF > Cluster.yaml
apiVersion: opstrace.com/v1alpha1
kind: Cluster
metadata:
  name: dev-cluster
spec:
  target: kind
  dns:
    acmeEmail: ""
    dns01Challenge: {}
    externalDNSProvider: {}
  gitlab:
    groupAllowedAccess: '*'
    groupAllowedSystemAccess: "6543"
    instanceUrl: https://gitlab.com
    authSecret:
      name: dev-secret
EOF
```

```bash
kubectl apply -f Cluster.yaml
```

Wait for the cluster to be ready:

```bash
kubectl wait --for=condition=ready cluster/dev-cluster --timeout=600s
```

Once the above command exits, the cluster is ready.

## Step 2: Enable Observability on a GitLab namespace you own

Navigate to a namespace you own in the connected GitLab instance, and copy the Group ID below the group name, for example:

![copy-group-id](./assets/copy-group-id.png)

Now open your browser to [http://localhost/-/{GroupID}](http://localhost/-/{GroupID}). In the above group, we'd open [http://localhost/-/14485840](http://localhost/-/14485840)

Follow the on screen instructions to enable observability for the namespace. This can take a couple of minutes if it's the first
time observability has been enabled for the root level namespace (GitLab.org) in the above example. There are many optimizations
we can make to reduce this provisioning time.

Once your namespace has been enabled and is ready, the page will automatically direct you to the GitLab Observability UI.

## Step 3: Send dummy traces data to GitLab Observability

[Follow this guide for sending traces to your namespace and checking them out in the UI.](./guides/user/sending-traces-locally.md)



## Step 4: Clean up

To tear down your locally running instance of GitLab Observability, run:

```bash
make destroy
```

## Known Issues


1. If you are running on apple silicon (M1/M2) chip, you might face incorrect target architecture for kind/node image while running `make kind`. take a look at this [issue](https://gitlab.com/gitlab-org/opstrace/opstrace/-/issues/1802)

    Run below command in order to fix it.
    create a new Dockerfile

    ```Dockerfile
    FROM --platform=arm64 kindest/node:v1.23.4
    RUN arch
    ```

    then build it via

    ```bash
    docker build -t tempkind .
    ```

    Then run to create a cluster.

    ```bash
    kind create cluster --image tempkind
    ```
