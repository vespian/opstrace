# Developing with Skaffold

[Skaffold](https://skaffold.dev/) is a great tool that allows for developing k8s using local workstation.

In comparision with [Telepresense](https://www.telepresence.io/) it allows for actually patching the deployments and running actual docker images in k8s, thus enabling much more accurate testing of the code
On the flip side, it is more complex to set up and use - the learning curve is pretty steep.

The configuration we have currently in repo is very limited, and applies only for scheduler.
If people start using it, we may can put more work into extending it and making it more universal.

## Develping scheduler

These are the steps that one can use to develop scheduler using Skaffold and local kind cluster:

* `make kind`
* `make -C scheduler install`
* `kubectl create secret generic postgres-secret -n kube-system --from-literal=endpoint='postgres://postgres:dev@pgpool-svc.postgres.svc.cluster.local:5432'`
* `skaffold dev`

Since then on, every time there is a change in scheduler's code, Skaffold will rebuild docker container and make scheduler deployment use it.
One can then e.g. inspect logs of the pod using `kubectl logs -f <schedulers-pod-name>`.

Many more things are possible, e.g. [remote debugging with Delve](https://skaffold.dev/docs/workflows/debug/#detailed-debugger-configuration-and-setup).
See the [Skaffold documentation](https://skaffold.dev/docs/workflows/) for more information
