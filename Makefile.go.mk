# Shared Makefile variables and targets for Go projects

# SHELLFLAGS compatible with bash and ash (alpine CI images).
# Exit on non-zero errors and undefined variables.
.SHELLFLAGS = -euc

export DOCKER_DEFAULT_PLATFORM ?= linux/amd64

# Registry address for pushing images to
DOCKER_IMAGE_REGISTRY ?= registry.gitlab.com/gitlab-org/opstrace/opstrace

define get_docker_image_name
	$(DOCKER_IMAGE_REGISTRY)/$(DOCKER_IMAGE_NAME):$(DOCKER_IMAGE_TAG)
endef

.PHONY: print-docker-image-name-tag
print-docker-image-name-tag:
	@echo $(call get_docker_image_name)

.PHONY: lint
lint: ## Run golangci-lint against the project
	golangci-lint run --allow-parallel-runners --timeout 5m

.PHONY: lint-code
lint-code: lint

.PHONY: fmt
fmt: ## Run go fmt against code.
	go fmt ./...

.PHONY: vet
vet: ## Run go vet against code.
	go vet ./...