<!-- markdownlint-disable MD041 -->
<!-- markdownlint-disable MD033 -->

<!-- TODO(joe): Add GitLab pipeline status badge -->

<img src="https://user-images.githubusercontent.com/19239758/97793010-00161b00-1ba3-11eb-949b-e62eae6fdb9c.png" width="350">

# The Open Source Observability Distribution

Opstrace is a secure, horizontally-scalable, open source observability platform installed in your cloud account.

Highlights:

* Horizontally **scalable**.
* Inexpensive **long-term** retention of observability data.
* Rigoriously **tested** end-to-end.
* Easy and reliable **upgrades**.
* **Secure** by default with [TLS](https://letsencrypt.org) and authenticated endpoints.
* **Easy to configure** with GUIs and APIs.

We walk on the shoulders of giants; Opstrace uses open source projects you know and love:

* [Cortex](https://github.com/cortexproject/cortex)
* [Grafana](https://github.com/grafana/grafana)
* [Kubernetes](https://github.com/kubernetes/kubernetes)
* [Prometheus](https://github.com/prometheus/prometheus)
* [Prometheus Operator](https://github.com/prometheus-operator/prometheus-operator)
* and many more

## Key Features

### Installation and Upgrades

Both installation and upgrades are initiated by a simple CLI command.
In this example, you can see an abbreviated installation and upgrade cycle:

```bash
$ cat << EOF > config.yaml
 tenants:
  - staging
  - prod
EOF
```

```text
$ ./opstrace create aws tracy -c config.yaml
...
info: create operation finished: tracy (aws)
info: Log in here: https://tracy.opstrace.io
```

A week later...

```text
$ curl -L https://go.opstrace.com/cli-latest-release-macos | tar xjf -
$ ./opstrace upgrade aws tracy -c config.yaml
...
info: upgrade operation finished for tracy (aws)
```

### Alert Management

Alertmanager can be difficult to configure, especially [for Cortex](https://cortexmetrics.io/docs/architecture/#alertmanager) when multiple tenants are used.
Opstrace configures a horizontally scalable Ruler and Alertmanager out of the box to support reliable alerts.
It also deploys a Grafana instance for each tenant, which can be now used to manage Cortex alerts thanks now to [Grafana 8's Unified Alerting](https://grafana.com/blog/2021/06/14/the-new-unified-alerting-system-for-grafana-everything-you-need-to-know/) feature.
Before, you'd have to manage each of these components independently, keeping them in sync manually.

<!-- markdownlint-disable MD044 -->
![opstrace ui alert overview](docs/assets/alerts-overview.jpg)

### Tenant Management

The Opstrace UI allows for dynamic [tenant creation and deletion](docs/guides/administrator/managing-tenants.md):

![tenants can be managed holistically](https://p95.p4.n0.cdn.getcloudapp.com/items/4gunxZZe/0d056830-92be-4417-aa90-21c8fa261f48.jpg?source=viewer&v=3ce7d21798ba7cf02869e35dfcfa70c6)

[Tenants](docs/references/concepts.md#tenants) provide isolation for logically separate entities.
For example, they can be used to represent different teams.
The Opstrace UI also allows for setting per-tenant configuration, which can be used for example to set write/read rate limits on a per-tenant basis.


## Quick Start

Install Opstrace in your own cloud account with our [quick start](https://go.opstrace.com/quickstart).
The Opstrace CLI uses your local credentials to setup the necessary cloud resources (e.g., a EKS or GKE cluster) and then deploys the Opstrace controller which orchestrates all of the app-level deployments.
For example:

```bash
opstrace create aws <choose_a_name> <<EOF
tenants:
  - dev
  - staging
  - prod
env_label: try_opstrace
cert_issuer: letsencrypt-prod
EOF
```

In addition to AWS we also support GCP.

See our configuration reference for details: [docs/references/configuration.md](docs/references/configuration.md).

Don't forget to clean up if you're done kicking the tires:

```bash
opstrace destroy aws <choose_a_name>
```

Note: add the `--region` flag to ensure the CLI can locate any dangling resources, should you run into any sort of trouble cleaning up on AWS (for example, after a failed install).

## Community

Please join us to learn more, get support, or contribute to the project.

* Join our [Community](https://about.gitlab.com/community/)
* Ask questions in our [Community Forum](https://forum.gitlab.com/c/observability/)
* For problems, review [issues](https://gitlab.com/gitlab-org/opstrace/opstrace/-/issues) and/or open a [bug report](https://gitlab.com/gitlab-org/opstrace/opstrace/-/issues/new?issuable_template=Bug%20report)
* Contribute a [feature proposal](https://gitlab.com/gitlab-org/opstrace/opstrace/-/issues/new?issuable_template=Proposal)


## Documentation

<!-- TODO: docs.gitlab.com -->
You can find the Opstrace documentation in [/docs](./docs).
We invite you to improve these docs together with us, and have a [corresponding guide](./docs/guides/contributor/writing-docs.md) for that.

## Contributing

We believe in a world where [everyone can contribute](https://about.gitlab.com/company/mission/).

Please join us and help with your contributions!

<!-- TODO: Unify into GitLab docs. -->
* Start by reading the [Contributing guide](./CONTRIBUTING.md) to become familiar with the process.
* Then review our [Development guide](./docs/guides/contributor/setting-up-your-dev-env.md) to learn how to set up your environment and build the code.

Take a look at our [high-level roadmap](./docs/references/roadmap.md) to see where we're heading.

IMPORTANT NOTE: We welcome contributions from developers of all backgrounds.
We encode that in our [Community Code of Conduct](https://about.gitlab.com/community/contribute/code-of-conduct/).
By participating in this project, you agree to abide by its terms.

### Directory reference

This `opstrace` repo currently holds several components of the Opstrace monitoring stack.
This organic per-directory listing should help new developers by providing some clues on where things are.
Please update this as things change.

* `.buildkite`: Old configuration for running CI in Buildkite. This has since been replaced with GitLab CI. Might be due for removal.
* `ci`: scripts/tooling for running tests in CI
* `clickhouse-operator`: A kubebuilder-based K8s Operator for managing one or more ClickHouse instances. This is structured as an independent project and may soon be broken out into a dedicated repo. We previously used the [Altinity ClickHouse Operator](https://github.com/Altinity/clickhouse-operator/) but in practice it required manually editing Pod templates in order to get a working cluster.
* `containers`: Dockerfile definitions for a few components. May go away soon?
* `docs`: User documentation. Much of this is likely overdue for updates as we develop the system, but there's still a lot of churn at the moment.
* `gatekeeper`: The authn/authz service for Opstrace, written in Go. This is designed integrated with a separate GitLab instance, which provides the user management.
* `go`: Smaller Go components that don't quite warrant their own subdirectory.
  * `cmd`: Standalone executables, run as their own containers in an Opstrace instance. These do not implement their own authn/authz, that is instead handled at the ingress.
    * `config`/`cortex`/`ddapi`: Multitenant proxy/adapter endpoints for interacting with Cortex metrics, either configuring Cortex alerts or sending metrics in either Prometheus or Datadog-agent format. These might be due for removal.
    * `tracing`: A more-or-less stock build of modules from [`opentelemetry-collector`](https://github.com/open-telemetry/opentelemetry-collector/)+[`opentelemetry-collector-contrib`](https://github.com/open-telemetry/opentelemetry-collector-contrib/). The modules are selected to support accepting otel-format data, converting it to Jaeger format, then sending it to a tenant Jaeger instance. Singletenant.
  * `pkg`: Library packages that may be shared by the above `cmd` executables, or by Go code elsewhere in the repo.
* `lib`: Old typescript libraries which support interacting with cloud providers (being replaced by `terraform`) and with K8s (being replaced by the various kubebuilder-based operators).
* `packages`: Old typescript projects which are being slowly replaced by new Go code.
  * `app`: Old WebUI/dashboards. Includes backend/frontend code.
  * `cli`: Commandline CLI for creating and managing Opstrace instances.
  * `config`/`schemas`/`tenants`: Utilities relating to configuring an Opstrace instance's dependencies, including AWS/GCP objects and local K8s ConfigMaps for instance config state
  * `controller-config`/`controller`: The old typescript-based Opstrace Operator. This is the first thing that gets deployed into a newly created Opstrace K8s cluster, where it then deploys everything else. Being replaced by a combination of `scheduler`/`tenant-operator` kubebuilder-based operators, but may still be relevant as a reference
  * `installer`/`uninstaller`/`upgrader`: Typescript logic for creating, destroying, or upgrading an Opstrace instance in either AWS or GCP. Being replaced by `terraform`.
* `scheduler`: A kubebuilder-based K8s operator that manages the Opstrace K8s instance as a whole. This deals with initial setup of tenants/groups that have enabled monitoring, which are then owned by `tenant-operator`.
* `tenant-operator`: A kubebuilder-based K8s operator that manages individual tenants within the Opstrace K8s instance, dealing with things like per-tenant dashboards.
* `terraform`: Logic for setting up a new Opstrace instance in GCP, replacing the previous hand-coded typescript CLI installer.
* `test`: Old typescript tests, including several E2E tests. Some of these may be portable into the individual Go projects, while others may be testing functionality that's no longer present.

# Privacy

<!-- TODO: Review the privacy policy requirement. -->
At Opstrace, we host a service that automatically provisions subdomains like `<my_name>.opstrace.io`.
This makes it easy and clean to use the URLs for both humans and machines.
To accomplish this safely, we require login via Auth0 subject to our [privacy poicy](https://go.opstrace.com/privacy-policy).

Get in touch with us to discuss support for custom domains!

## Security Reports

Please report suspected security vulnerabilities by following the [disclosure process on the GitLab.com website](https://about.gitlab.com/security/disclosure/).

