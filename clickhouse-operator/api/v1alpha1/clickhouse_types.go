/*
Copyright 2022.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package v1alpha1

import (
	// K8s Go types defined here:
	// https://github.com/kubernetes/kubernetes/blob/master/pkg/apis/core/types.go
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/resource"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// ClickHouseAdmin defines a managed admin user to be configured in the ClickHouse instance.
// This is used to define "bootstrap" admin users that can then create other limited users.
type ClickHouseAdmin struct {
	// Name is the username of the user.
	Name string `json:"name"`

	// SecretKeyRef points to the secret value where the admin user's password is held.
	// The secret must be in the same K8s namespace as the ClickHouse instance referencing it.
	// This only applies upon user creation and later changes are not automatically synchronized.
	SecretKeyRef corev1.SecretKeySelector `json:"secretKeyRef"`
}

// ClickHouseProxy describes proxy settings of the ClickHouse instance and is
// used to configure and control CHProxy.
type ClickHouseProxy struct {
	// DebugEnabled enables debug logging.
	// +optional
	// +kubebuilder:default=false
	DebugEnabled *bool `json:"debugEnabled,omitempty"`

	// Replicas is the number of desired proxy replicas.
	// +optional
	// +kubebuilder:default=1
	Replicas *int32 `json:"replicas,omitempty"`

	// AllowedNetworks is the list of networks that access is allowed from. Each
	// list item could be IP address or subnet mask
	// +optional
	AllowedNetworks []string `json:"allowedNetworks,omitempty"`

	// ManagementNetworks is the list of networks that monitoring and management
	// access is allowed from. Each list item could be IP address or subnet mask.
	// +optional
	ManagementNetworks []string `json:"managementNetworks,omitempty"`

	// Listeners describes the server listeners configuration.
	Listeners ClickHouseProxyListeners `json:"listeners"`

	// Users is the list of allowed users and their access policies and quotas
	// which requests will be proxied to ClickHouse.
	// +optional
	Users []ClickHouseProxyUser `json:"users,omitempty"`
}

// ClickHouseProxyListeners describes the server listeners of CHProxy.
type ClickHouseProxyListeners struct {
	// HTTP server listener configuration.
	HTTP ClickHouseProxyHTTPListener `json:"http,omitempty"`
}

// ClickHouseProxyHTTPListener describes the HTTP server configuration of CHProxy.
type ClickHouseProxyHTTPListener struct {
	// Address is the bind address and port of the listener.
	Address string `json:"address"`

	// ReadTimeout is the maximum duration for reading the entire request,
	// including the body.
	// +optional
	ReadTimeout *string `json:"readTimeout,omitempty"`

	// WriteTimeout is the maximum duration before timing out writes of the
	// response.
	// +optional
	WriteTimeout *string `json:"writeTimeout,omitempty"`

	// IdleTimeout is the maximum amount of time to wait for the next request.
	// +optional
	IdleTimeout *string `json:"idleTimeout,omitempty"`
}

// ClickHouseProxyUser describes a CHProxy user and its access policies and
// quotas.
type ClickHouseProxyUser struct {
	// Name is the proxy user name.
	Name string `json:"name"`

	// PasswordSecret is the reference to the secret that contains the password.
	// +optional
	PasswordSecret *corev1.SecretKeySelector `json:"passwordSecret,omitempty"`

	// ToUser must match with name of the user from cluster config, specified
	// by `adminUsers`, whom credentials will be used for proxying request to
	// ClickHouse instance.
	// +optional
	ToUser *string `json:"toUser,omitempty"`

	// MaxConcurrentQueries is the maximum number of concurrently running
	// queries for user.
	// +optional
	MaxConcurrentQueries *int32 `json:"maxConcurrentQueries,omitempty"`

	// MaxExecutionTime is the maximum duration of query execution for user.
	// +optional
	MaxExecutionTime *string `json:"maxExecutionTime,omitempty"`

	// RequestsPerMinute is the maximum number of requests per minute for user.
	// +optional
	RequestsPerMinute *int32 `json:"requestsPerMinute,omitempty"`

	// MaxQueueSize is the maximum number of requests waiting for execution in the queue.
	MaxQueueSize *int32 `json:"maxQueueSize,omitempty"`

	// MaxQueueTime is the maximum duration the request may wait in the queue.
	// +optional
	MaxQueueTime *string `json:"maxQueueTime,omitempty"`

	// DenyHTTP indicates whether to deny HTTP connections for this user.
	// +optional
	DenyHTTP *bool `json:"denyHTTP,omitempty"`

	// DenyHTTPS indicates whether to deny HTTPS connections for this user.
	// +optional
	DenyHTTPS *bool `json:"denyHTTPS,omitempty"`

	// AllowCORS indicates whether to allow CORS requests for this user.
	// +optional
	AllowCORS *bool `json:"allowCORS,omitempty"`
}

// ClickhouseObjectStorageBackend defines the backend for the object storage to be used by Clickhouse.
// By Default it is S3.
// +kubebuilder:validation:Enum=S3;GCS
type ClickhouseObjectStorageBackend string

// ClickHouseObjectStorage describes configuration for applicable object storage backend(S3/GCS).
type ClickHouseObjectStorage struct {
	// Backend provides the object storage, for example S3/GCS.
	// Default is S3.
	Backend ClickhouseObjectStorageBackend `json:"backend"`

	// EndpointURL is currently S3 endpoint URL in path or virtual hosted styles.
	// Endpoint URL should contain a bucket and root path to store data.
	// Note(Arun): GCS specific URLs should adhere to the same specification.
	EndpointURL string `json:"endpointURL"`

	// AccessKeyIDSecret refers to the secret for the access key ID.
	// The secret must be in the same K8s namespace as the ClickHouse instance referencing it.
	// This only applies upon user creation and later changes are not automatically synchronized.
	AccessKeyIDSecret *corev1.SecretKeySelector `json:"accessKeyIDSecret"`

	// AccessKeySecret refers to the secret for the access key.
	// The secret must be in the same K8s namespace as the ClickHouse instance referencing it.
	// This only applies upon user creation and later changes are not automatically synchronized.
	AccessKeySecret *corev1.SecretKeySelector `json:"accessKeySecret"`

	// Region is the S3/GCS region name where the bucket is located.
	// +optional
	Region *string `json:"region,omitempty"`

	// CustomerKey refers to the secret for the customer key for server side encryption.
	// +optional
	CustomerKey *corev1.SecretKeySelector `json:"customerKey,omitempty"`
}

// ClickHouseSpec defines the desired state of ClickHouse.
type ClickHouseSpec struct {
	// Image is the ClickHouse docker image to be used for nodes.
	Image string `json:"image"`

	// Replicas is the number of desired nodes to run.
	// The instance will always be deployed in distributed mode, even if replicas=1.
	// Optional to discriminate between explicit zero and not set.
	// Defaults to 1.
	// +optional
	// +kubebuilder:default=1
	Replicas *int32 `json:"replicas,omitempty"`

	// StorageSize is the amount of block storage capacity to allocate per replica.
	StorageSize resource.Quantity `json:"storageSize"`

	// Affinity has any restrictions on replica placement in the cluster.
	// This is copied through to the nodes.
	// +optional
	Affinity *corev1.Affinity `json:"affinity,omitempty"`

	// StorageClass is the storage class to use for data volumes
	// +optional
	StorageClass *string `json:"storageClass,omitempty"`

	// InternalSSL is a flag to enable ssl between server/clients of the Clickhouse deployment.
	// Defaults to false. Assumes that certmanager is installed in the cluster.
	// +optional
	// +kubebuilder:default=false
	InternalSSL *bool `json:"internalSSL,omitempty"`

	// AdminUsers contains one or more admin users (access_management=1) to create in the instance.
	// Additional non-admin users may be added via ClickHouse APIs using these credentials.
	// +optional
	AdminUsers []ClickHouseAdmin `json:"adminUsers,omitempty"`

	// Proxy contains the CHProxy configuration and control.
	// +optional
	Proxy *ClickHouseProxy `json:"proxy,omitempty"`

	// ObjectStorage is the configuration for the object storage that will be used by ClickHouse.
	// +optional
	ObjectStorage *ClickHouseObjectStorage `json:"objectStorage,omitempty"`
}

// ClickHouseClusterStatus gives general state of cluster setup.
type ClickHouseClusterStatus string

// ClickHouseStatus defines the observed state of ClickHouse.
type ClickHouseStatus struct {
	// Status is general status of cluster availability.
	// +optional
	Status ClickHouseClusterStatus `json:"status,omitempty"`

	// ReadyShards is the number of StatefulSet shards that have a ready condition.
	// A Single shard is comprised of a number of replicas.
	// +optional
	ReadyShards int32 `json:"readyShards,omitempty"`

	// ReadyReplicas is the number of StatefulSet cluster replicas that have a ready condition.
	// Considered ready when the StatefulSet readyReplicas equals its spec.replicas.
	// +optional
	ReadyReplicas int32 `json:"readyReplicas,omitempty"`
}

//+kubebuilder:object:root=true
//+kubebuilder:subresource:status
//+kubebuilder:printcolumn:name="Status",type=string,JSONPath=`.status.status`
//+kubebuilder:printcolumn:name="Replicas",type=integer,JSONPath=`.spec.replicas`
//+kubebuilder:printcolumn:name="Ready Replicas",type=integer,JSONPath=`.status.readyReplicas`

// ClickHouse is the Schema for the clickhouses API.
type ClickHouse struct {
	metav1.TypeMeta `json:",inline"`

	// Standard object's metadata.
	// +optional
	metav1.ObjectMeta `json:"metadata,omitempty"`

	// Spec of desired behaviour.
	// +optional
	Spec ClickHouseSpec `json:"spec,omitempty"`

	// Status of the ClickHouse cluster
	// +optional
	Status ClickHouseStatus `json:"status,omitempty"`
}

//+kubebuilder:object:root=true

// ClickHouseList contains a list of ClickHouse.
type ClickHouseList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata,omitempty"`
	Items           []ClickHouse `json:"items"`
}

func init() {
	SchemeBuilder.Register(&ClickHouse{}, &ClickHouseList{})
}
