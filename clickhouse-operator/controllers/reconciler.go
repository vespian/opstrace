package controllers

import (
	"context"
	"fmt"

	"github.com/go-logr/logr"
	"k8s.io/apimachinery/pkg/runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/controller/controllerutil"

	clickhousev1alpha1 "gitlab.com/gitlab-org/opstrace/opstrace/clickhouse-operator/api/v1alpha1"
)

// KubernetesResource abstracts a K8s resource for reconciliation.
// The mutator is used in CreateOrUpdate to maintain desired state.
type KubernetesResource struct {
	obj     client.Object
	mutator controllerutil.MutateFn
}

// Reconciler is used to reconcile any number of resources.
type Reconciler struct {
	log    logr.Logger
	client client.Client
	crd    *clickhousev1alpha1.ClickHouse
	scheme *runtime.Scheme
}

// Reconcile will create or update the resource and set the controller ownership.
func (r *Reconciler) Reconcile(ctx context.Context, kr *KubernetesResource) error {
	obj := kr.obj

	// Set up garbage collection. The object (resource.obj) will be
	// automatically deleted when the owner (clickhouse) is deleted.
	err := controllerutil.SetControllerReference(r.crd, obj, r.scheme)
	if err != nil {
		r.log.Error(
			err,
			"failed to set owner reference on resource",
			"kind", obj.GetObjectKind().GroupVersionKind().Kind,
			"name", obj.GetName(),
		)
		return fmt.Errorf("reconcile set owner ref: %w", err)
	}

	op, err := controllerutil.CreateOrUpdate(ctx, r.client, obj, kr.mutator)
	if err != nil {
		r.log.Error(
			err,
			"failed to reconcile resource",
			"kind", obj.GetObjectKind().GroupVersionKind().Kind,
			"name", obj.GetName(),
		)
		return fmt.Errorf("reconcile create or update: %w", err)
	}

	r.log.Info(
		"Reconcile successful",
		"operation", op,
		"kind", obj.GetObjectKind().GroupVersionKind().Kind,
		"name", obj.GetName(),
	)

	return nil
}
