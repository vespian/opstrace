package controllers

import (
	"fmt"

	clickhousev1alpha1 "gitlab.com/gitlab-org/opstrace/opstrace/clickhouse-operator/api/v1alpha1"
)

// clickHouseCluster represents a full cluster based on CR.
type clickHouseCluster struct {
	*clickhousev1alpha1.ClickHouse

	// Shards is desired shard count (currently always 1).
	Shards int32

	// Replicas is desired replica count currently, per shard.
	Replicas int32

	// Hosts represents the service names of all replicas.
	// Hosts are in format {name}-{shard}-{replica}.
	Hosts []string

	// ConfigXML is general config XML for the server.
	ConfigXML string
	// UsersXML is user/profile/quota specific config.
	UsersXML string
	// ProxyYAML is optional config loaded from the CHProxy.
	ProxyYAML string
	// ClientConfigXML is config XML for the clickhouse-client that is available on the pods.
	ClientConfigXML string

	// InternalSSL denotes if SSL should be enabled in the cluster.
	// Should be read from ClickHouse.Spec.Deploy.InternalSSL
	InternalSSL bool
	// DHParams is optional dh parameters config that will be loaded to Cluster if InternalSSL is enabled.
	// Only applicable when InternalSSL is true.
	DHParams string

	// ObjectStorageEnabled denotes if S3/GCS endpoints are to be used by Clickhouse.
	ObjectStorageEnabled bool
}

func newClickHouseCluster(cr *clickhousev1alpha1.ClickHouse) (*clickHouseCluster, error) {
	var (
		// Note(joe): only one shard currently supported
		shards    = 1
		replicas  = 0
		chCluster = &clickHouseCluster{
			ClickHouse: cr,
		}
	)
	if cr.Spec.Replicas != nil {
		replicas = int(*cr.Spec.Replicas)
	}

	// hostnames in format {name}-{shard}-{replica}
	hosts := make([]string, shards*replicas)
	for s := 0; s < shards; s++ {
		for r := 0; r < replicas; r++ {
			hosts[s*replicas+r] = fmt.Sprintf("%s-%d-%d", cr.Name, s, r)
		}
	}

	chCluster.Hosts = hosts
	chCluster.Replicas = int32(replicas)
	chCluster.Shards = int32(shards)

	if cr.Spec.InternalSSL != nil && *cr.Spec.InternalSSL {
		chCluster.InternalSSL = *cr.Spec.InternalSSL
	}

	if cr.Spec.ObjectStorage != nil {
		chCluster.ObjectStorageEnabled = true
	}

	configsXML, err := render("config.tmpl.xml", chCluster)
	if err != nil {
		return nil, fmt.Errorf("render config: %w", err)
	}
	usersXML, err := render("users.tmpl.xml", chCluster)
	if err != nil {
		return nil, fmt.Errorf("render users: %w", err)
	}
	clientXML, err := render("client-config.tmpl.xml", chCluster)
	if err != nil {
		return nil, fmt.Errorf("render client config: %w", err)
	}
	chCluster.ConfigXML = configsXML
	chCluster.UsersXML = usersXML
	chCluster.ClientConfigXML = clientXML

	if chCluster.InternalSSL {
		chCluster.DHParams, err = render("dhparam.tmpl.txt", chCluster)
		if err != nil {
			return nil, fmt.Errorf("render dhparams: %w", err)
		}
	}

	return chCluster, nil
}

func (c *clickHouseCluster) proxyEnabled() bool {
	return c.Spec.Proxy != nil
}

func (c *clickHouseCluster) configureProxy(passwords map[string]string) error {
	if len(c.Spec.Proxy.AllowedNetworks) == 0 {
		c.Spec.Proxy.AllowedNetworks = []string{"10.0.0.0/8"}
	}

	if len(c.Spec.Proxy.ManagementNetworks) == 0 {
		c.Spec.Proxy.ManagementNetworks = []string{"10.0.0.0/8"}
	}

	if c.Spec.Proxy.Listeners.HTTP.Address == "" {
		c.Spec.Proxy.Listeners.HTTP.Address = ":80"
	}

	for i, u := range c.Spec.Proxy.Users {
		if (u.ToUser == nil || *u.ToUser == "") && len(c.Spec.AdminUsers) > 0 {
			adminUser := c.Spec.AdminUsers[0].Name
			c.Spec.Proxy.Users[i].ToUser = &adminUser
		}
	}

	proxyYAML, err := render("proxy.tmpl.yaml", map[string]interface{}{
		"CH":        c,
		"Passwords": passwords,
	})
	if err != nil {
		return fmt.Errorf("render proxy config: %w", err)
	}
	c.ProxyYAML = proxyYAML

	return nil
}
