package controllers

import (
	"fmt"

	certmanagerv1 "github.com/cert-manager/cert-manager/pkg/apis/certmanager/v1"
	cmmeta "github.com/cert-manager/cert-manager/pkg/apis/meta/v1"
	v1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

const (
	clickhouseCertName       = "serving-cert"
	clickhouseCertSecretName = "clickhouse-cert"
)

// Note(Arun): Improvement would be to be able to fetch the certificate/key pairs from external references.

func certificate(ch *clickHouseCluster) *certmanagerv1.Certificate {
	return &certmanagerv1.Certificate{
		ObjectMeta: v1.ObjectMeta{
			Name:      clickhouseCertName,
			Namespace: ch.Namespace,
		},
		Spec: certmanagerv1.CertificateSpec{
			DNSNames: []string{
				fmt.Sprintf("%s.%s.svc", ch.Name, ch.Namespace),
				fmt.Sprintf("%s.%s.svc.cluster.local", ch.Name, ch.Namespace),
			},
			// Note(Arun): Although this can be considered a code smell but as we open connections on this address,
			// this is necessary to make the tls verification succeed when connecting directly via this address.
			IPAddresses: []string{"0.0.0.0"},
			IssuerRef: cmmeta.ObjectReference{
				Kind: "Issuer",
				Name: caIssuerName,
			},
			SecretName: clickhouseCertSecretName,
			Subject: &certmanagerv1.X509Subject{
				OrganizationalUnits: []string{"clickhouse-operator"},
			},
		},
	}
}

func certificateMutator(ch *clickHouseCluster, current *certmanagerv1.Certificate) {
	cert := certificate(ch)
	current.Name = clickhouseCertName
	current.Spec.DNSNames = cert.Spec.DNSNames
	current.Spec.IPAddresses = cert.Spec.IPAddresses
	current.Spec.IssuerRef = cert.Spec.IssuerRef
	current.Spec.SecretName = cert.Spec.SecretName
	current.Spec.Subject = cert.Spec.Subject
}
