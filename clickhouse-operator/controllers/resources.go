package controllers

import (
	"fmt"
	"strconv"
	"strings"

	appsv1 "k8s.io/api/apps/v1"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/resource"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/util/intstr"
	"k8s.io/utils/pointer"
	ctrl "sigs.k8s.io/controller-runtime"

	clickhousev1alpha1 "gitlab.com/gitlab-org/opstrace/opstrace/clickhouse-operator/api/v1alpha1"
)

const (
	dhParamsKey = "dhparam.pem"
)

func newClickHouseConfigMaps(
	req ctrl.Request,
	ch *clickHouseCluster,
) []*KubernetesResource {
	kr := make([]*KubernetesResource, 0)

	labels := map[string]string{
		"app":  "clickhouse",
		"name": req.Name,
	}

	kr = append(kr, newConfigMap(
		req.Namespace,
		req.Name,
		labels,
		map[string]string{
			"config.xml":        ch.ConfigXML,
			"users.xml":         ch.UsersXML,
			"client-config.xml": ch.ClientConfigXML,
		},
	))

	return kr
}

func newProxyConfig(ch *clickHouseCluster, config string) []*KubernetesResource {
	kr := make([]*KubernetesResource, 0)

	labels := map[string]string{
		"app":  "clickhouse-proxy",
		"name": ch.Name,
	}

	kr = append(kr, newSecret(
		ch.Namespace,
		fmt.Sprintf("%s-proxy-config", ch.Name),
		labels,
		map[string]string{
			"config.yaml": config,
		},
	))

	return kr
}

func newConfigMap(namespace string, name string, labels map[string]string,
	data map[string]string) *KubernetesResource {
	cm := &corev1.ConfigMap{ObjectMeta: metav1.ObjectMeta{Name: name, Namespace: namespace, Labels: labels}}
	return &KubernetesResource{
		obj: cm,
		mutator: func() error {
			cm.Labels = labels
			cm.Data = data
			return nil
		},
	}
}

func newSecret(namespace string, name string, labels map[string]string,
	data map[string]string) *KubernetesResource {
	st := &corev1.Secret{ObjectMeta: metav1.ObjectMeta{Name: name, Namespace: namespace, Labels: labels}}
	return &KubernetesResource{
		obj: st,
		mutator: func() error {
			st.Labels = labels
			st.StringData = data
			return nil
		},
	}
}

func newClickHouseServices(
	req ctrl.Request,
	ch *clickHouseCluster,
) []*KubernetesResource {
	kr := make([]*KubernetesResource, 0)

	clientPorts := []corev1.ServicePort{
		{
			Name:       "http",
			Port:       8123,
			TargetPort: intstr.FromString("http"),
		},
		{
			Name:       "client",
			Port:       9000,
			TargetPort: intstr.FromString("client"),
		},
	}

	nodePorts := []corev1.ServicePort{
		{
			Name:       "interserver",
			Port:       9009,
			TargetPort: intstr.FromString("interserver"),
		},
		// Improvement: Remove extra keeper port if SSL is enabled
		{
			Name:       "keeper",
			Port:       2181,
			TargetPort: intstr.FromString("keeper"),
		},
		{
			Name:       "raft",
			Port:       9444,
			TargetPort: intstr.FromString("raft"),
		},
		{
			Name:       "metrics",
			Port:       8001,
			TargetPort: intstr.FromString("metrics"),
		},
	}

	if ch.InternalSSL {
		clientPorts = append(clientPorts, []corev1.ServicePort{
			{
				Name:       "https",
				Port:       8443,
				TargetPort: intstr.FromString("https"),
			},
			{
				Name:       "client-secure",
				Port:       9440,
				TargetPort: intstr.FromString("client-secure"),
			},
		}...)
		nodePorts = append(nodePorts, []corev1.ServicePort{
			{
				Name:       "int-serv-secure",
				Port:       9010,
				TargetPort: intstr.FromString("int-serv-secure"),
			},
			{
				Name:       "keeper-secure",
				Port:       9281,
				TargetPort: intstr.FromString("keeper-secure"),
			},
		}...)
	}

	nodePorts = append(nodePorts, clientPorts...)

	// Create shared "<name>" Service for access by clients
	clusterLabels := map[string]string{
		"app":  "clickhouse",
		"name": req.Name,
	}
	kr = append(kr, newService(
		req.Namespace,
		req.Name,
		clusterLabels,
		clientPorts,
		clusterLabels,
		"",
		false,
	))

	// Create per-node "host" Services to allow direct addressing between nodes.
	// Note(joe): headless services using PublishNotReadyAddresses=true to allow pre-ready DNS access.
	for i, h := range ch.Hosts {
		nodeLabels := map[string]string{
			"app":     "clickhouse",
			"name":    req.Name,
			"replica": strconv.Itoa(i),
		}
		kr = append(kr, newService(
			req.Namespace,
			h,
			nodeLabels,
			nodePorts,
			nodeLabels,
			"None",
			true,
		))
	}

	return kr
}

func newProxyServices(
	ch *clickHouseCluster,
) []*KubernetesResource {
	labels := map[string]string{
		"app":  "clickhouse-proxy",
		"name": ch.Name,
	}

	var port = 80
	addr := strings.Split(ch.Spec.Proxy.Listeners.HTTP.Address, ":")
	if len(addr) > 1 {
		if i, err := strconv.ParseInt(addr[1], 10, 0); err == nil {
			port = int(i)
		}
	}

	ports := []corev1.ServicePort{
		{
			Name:       "http",
			Port:       int32(port),
			TargetPort: intstr.FromString("http"),
		},
	}

	return []*KubernetesResource{
		newService(
			ch.Namespace,
			fmt.Sprintf("%s-proxy", ch.Name),
			map[string]string{},
			ports,
			labels,
			"",
			false,
		),
	}
}

func newService(namespace string, name string, labels map[string]string,
	ports []corev1.ServicePort, selector map[string]string, clusterIP string,
	publishNotReadyAddresses bool) *KubernetesResource {
	svc := &corev1.Service{ObjectMeta: metav1.ObjectMeta{Name: name, Namespace: namespace, Labels: labels}}
	return &KubernetesResource{
		obj: svc,
		mutator: func() error {
			svc.Labels = labels
			svc.Spec.Ports = ports
			svc.Spec.Selector = selector
			if len(clusterIP) > 0 {
				// Avoid triggering a service change if the spec ClusterIP is already assigned
				svc.Spec.ClusterIP = clusterIP
			}
			svc.Spec.PublishNotReadyAddresses = publishNotReadyAddresses
			return nil
		},
	}
}

func newClickHouseStatefulSets(
	req ctrl.Request,
	ch *clickHouseCluster,
) []*KubernetesResource {
	kr := make([]*KubernetesResource, 0)

	for i, h := range ch.Hosts {
		// 'replica=#' used by granular per-pod Services
		labels := map[string]string{
			"app":     "clickhouse",
			"name":    req.Name,
			"replica": strconv.Itoa(i),
			"shard":   strconv.Itoa(0),
		}
		kr = append(kr, newClickHouseStatefulSet(
			req.Namespace,
			h,
			i,
			ch,
			labels,
		))
	}

	return kr
}

func newClickHouseStatefulSet(
	namespace string,
	host string,
	replica int,
	ch *clickHouseCluster,
	labels map[string]string,
) *KubernetesResource {
	sts := &appsv1.StatefulSet{
		ObjectMeta: metav1.ObjectMeta{Name: host, Namespace: namespace},
	}

	return &KubernetesResource{
		obj: sts,
		mutator: func() error {
			env := make([]corev1.EnvVar, 0)
			// For each admin user, create a password envvar to be referenced by users.xml
			for _, user := range ch.Spec.AdminUsers {
				// TODO(joe): passwords should not be in env vars.
				// We should load the secret and sha256_hex the password.
				// See https://clickhouse.com/docs/en/operations/settings/settings-users/#user-namepassword.
				env = append(env, corev1.EnvVar{
					Name: fmt.Sprintf("ADMIN_PASSWORD_%s", user.Name),
					ValueFrom: &corev1.EnvVarSource{
						SecretKeyRef: &corev1.SecretKeySelector{
							LocalObjectReference: corev1.LocalObjectReference{Name: user.SecretKeyRef.Name},
							Key:                  user.SecretKeyRef.Key,
						},
					},
				})
			}
			env = append(env, []corev1.EnvVar{
				// Add the pod's replica index, referenced in config.tmpl.xml
				{
					Name:  "REPLICA",
					Value: strconv.Itoa(replica),
				},
				// Add host service name so this can be referenced in config
				{
					Name:  "HOST",
					Value: host,
				},
			}...)

			if ch.ObjectStorageEnabled && ch.Spec.ObjectStorage.Backend == clickhousev1alpha1.BackendS3 {
				env = append(env, []corev1.EnvVar{
					{
						Name: "ACCESS_KEY_ID",
						ValueFrom: &corev1.EnvVarSource{
							SecretKeyRef: &corev1.SecretKeySelector{
								LocalObjectReference: corev1.LocalObjectReference{Name: ch.Spec.ObjectStorage.AccessKeyIDSecret.Name},
								Key:                  ch.Spec.ObjectStorage.AccessKeyIDSecret.Key,
							},
						},
					},
					{
						Name: "SECRET_ACCESS_KEY",
						ValueFrom: &corev1.EnvVarSource{
							SecretKeyRef: &corev1.SecretKeySelector{
								LocalObjectReference: corev1.LocalObjectReference{Name: ch.Spec.ObjectStorage.AccessKeySecret.Name},
								Key:                  ch.Spec.ObjectStorage.AccessKeySecret.Key,
							},
						},
					},
				}...)

				if ch.Spec.ObjectStorage.CustomerKey != nil {
					env = append(env, corev1.EnvVar{
						Name: "SSE_CUSTOMER_KEY",
						ValueFrom: &corev1.EnvVarSource{
							SecretKeyRef: &corev1.SecretKeySelector{
								LocalObjectReference: corev1.LocalObjectReference{Name: ch.Spec.ObjectStorage.CustomerKey.Name},
								Key:                  ch.Spec.ObjectStorage.CustomerKey.Key,
							},
						},
					})
				}
				if ch.Spec.ObjectStorage.Region != nil {
					env = append(env, corev1.EnvVar{
						Name:  "STORAGE_REGION",
						Value: *ch.Spec.ObjectStorage.Region,
					})
				}
			}

			// Each StatefulSet has a matching per-node Service with the same name
			sts.Spec.ServiceName = host
			// Each node is being deployed in its own StatefulSet
			var replicas int32 = 1
			sts.Spec.Replicas = &replicas
			sts.Spec.PodManagementPolicy = appsv1.OrderedReadyPodManagement
			sts.Spec.Selector = &metav1.LabelSelector{
				MatchLabels: labels,
			}
			sts.Spec.Template.Labels = labels
			sts.Spec.Template.Spec.Containers = []corev1.Container{
				{
					Name:            "clickhouse",
					Image:           ch.Spec.Image,
					ImagePullPolicy: corev1.PullIfNotPresent,
					Env:             env,
					Ports: []corev1.ContainerPort{
						{
							Name:          "http",
							ContainerPort: 8123,
						},
						{
							Name:          "client",
							ContainerPort: 9000,
						},
						{
							Name:          "interserver",
							ContainerPort: 9009,
						},
						{
							Name:          "keeper",
							ContainerPort: 2181,
						},
						{
							Name:          "raft",
							ContainerPort: 9444,
						},
						{
							Name:          "metrics",
							ContainerPort: 8001,
						},
					},
					// NOTE(joe): just use readiness probe, we don't want to restart pods in this cluster.
					// Readiness in combination with headless PublishNotReadyAddresses service allows
					// cluster to form while correctly reporting pod status.
					ReadinessProbe: &corev1.Probe{
						ProbeHandler: corev1.ProbeHandler{
							HTTPGet: &corev1.HTTPGetAction{
								Path:   "/ping",
								Port:   intstr.FromString("http"),
								Scheme: corev1.URISchemeHTTP,
							},
						},
						InitialDelaySeconds: 5,
						TimeoutSeconds:      1,
						PeriodSeconds:       10,
						SuccessThreshold:    1,
						FailureThreshold:    3,
					},
					SecurityContext: &corev1.SecurityContext{
						Capabilities: &corev1.Capabilities{
							Add:  []corev1.Capability{"IPC_LOCK", "SYS_NICE"},
							Drop: []corev1.Capability{"ALL"},
						},
					},
					VolumeMounts: []corev1.VolumeMount{
						{
							Name:      "configd",
							MountPath: "/etc/clickhouse-server/config.d",
						},
						{
							Name:      "usersd",
							MountPath: "/etc/clickhouse-server/users.d",
						},
						{
							Name:      "data",
							MountPath: "/var/lib/clickhouse",
						},
						{
							Name:      "logs",
							MountPath: "/var/log/clickhouse-server",
						},
						{
							Name:      "cliendconfigd",
							MountPath: "/etc/clickhouse-client/config.d",
						},
					},
				},
			}
			var uidGID int64 = 101
			sts.Spec.Template.Spec.SecurityContext = &corev1.PodSecurityContext{
				RunAsUser:  &uidGID,
				RunAsGroup: &uidGID,
				FSGroup:    &uidGID,
			}
			sts.Spec.Template.Spec.TerminationGracePeriodSeconds = pointer.Int64Ptr(30)
			sts.Spec.Template.Spec.Volumes = []corev1.Volume{
				{
					Name: "configd",
					VolumeSource: corev1.VolumeSource{
						ConfigMap: &corev1.ConfigMapVolumeSource{
							LocalObjectReference: corev1.LocalObjectReference{Name: ch.Name},
							Items: []corev1.KeyToPath{
								{
									Key:  "config.xml",
									Path: "config.xml",
								},
							},
						},
					},
				},
				{
					Name: "usersd",
					VolumeSource: corev1.VolumeSource{
						ConfigMap: &corev1.ConfigMapVolumeSource{
							LocalObjectReference: corev1.LocalObjectReference{Name: ch.Name},
							Items: []corev1.KeyToPath{
								{
									Key:  "users.xml",
									Path: "users.xml",
								},
							},
						},
					},
				},
				{
					Name: "data",
					VolumeSource: corev1.VolumeSource{
						PersistentVolumeClaim: &corev1.PersistentVolumeClaimVolumeSource{
							ClaimName: "data",
						},
					},
				},
				{
					Name: "logs",
					VolumeSource: corev1.VolumeSource{
						PersistentVolumeClaim: &corev1.PersistentVolumeClaimVolumeSource{
							ClaimName: "logs",
						},
					},
				},
				{
					Name: "cliendconfigd",
					VolumeSource: corev1.VolumeSource{
						ConfigMap: &corev1.ConfigMapVolumeSource{
							LocalObjectReference: corev1.LocalObjectReference{Name: ch.Name},
							Items: []corev1.KeyToPath{
								{
									Key:  "client-config.xml",
									Path: "config.xml",
								},
							},
						},
					},
				},
			}
			sts.Spec.VolumeClaimTemplates = []corev1.PersistentVolumeClaim{
				{
					ObjectMeta: metav1.ObjectMeta{Name: "data"},
					Spec: corev1.PersistentVolumeClaimSpec{
						StorageClassName: ch.Spec.StorageClass,
						AccessModes:      []corev1.PersistentVolumeAccessMode{corev1.ReadWriteOnce},
						Resources: corev1.ResourceRequirements{
							Requests: corev1.ResourceList{
								corev1.ResourceStorage: ch.Spec.StorageSize,
							},
						},
					},
				},
				// Separate volume to avoid interruption to logs if data volume is full
				{
					ObjectMeta: metav1.ObjectMeta{Name: "logs"},
					Spec: corev1.PersistentVolumeClaimSpec{
						StorageClassName: ch.Spec.StorageClass,
						AccessModes:      []corev1.PersistentVolumeAccessMode{corev1.ReadWriteOnce},
						Resources: corev1.ResourceRequirements{
							Requests: corev1.ResourceList{
								corev1.ResourceStorage: resource.MustParse("1Gi"),
							},
						},
					},
				},
			}

			if ch.InternalSSL {
				// assumption that the clickhouse container is added beforehand
				clickhouseContainer := sts.Spec.Template.Spec.Containers[0]
				clickhouseContainer.Ports = append(clickhouseContainer.Ports, []corev1.ContainerPort{
					{
						Name:          "https",
						ContainerPort: 8443,
					},
					{
						Name:          "client-secure",
						ContainerPort: 9440,
					},
					{
						Name:          "int-serv-secure",
						ContainerPort: 9010,
					},
					{
						Name:          "keeper-secure",
						ContainerPort: 9281,
					},
				}...)

				clickhouseContainer.VolumeMounts = append(clickhouseContainer.VolumeMounts, corev1.VolumeMount{
					Name:      "ssl",
					MountPath: "/etc/clickhouse-server/ssl",
				})
				sts.Spec.Template.Spec.Containers[0] = clickhouseContainer

				sts.Spec.Template.Spec.Volumes = append(sts.Spec.Template.Spec.Volumes, corev1.Volume{
					Name: "ssl",
					VolumeSource: corev1.VolumeSource{
						Projected: &corev1.ProjectedVolumeSource{
							Sources: []corev1.VolumeProjection{
								{
									Secret: &corev1.SecretProjection{
										LocalObjectReference: corev1.LocalObjectReference{Name: clickhouseCertSecretName},
										Items: []corev1.KeyToPath{
											{Key: "ca.crt", Path: "ca.crt"},
											{Key: "tls.crt", Path: "tls.crt"},
											{Key: "tls.key", Path: "tls.key"},
										},
									},
								},
								{
									Secret: &corev1.SecretProjection{
										LocalObjectReference: corev1.LocalObjectReference{Name: fmt.Sprintf("%s-dhparam", ch.Name)},
										Items: []corev1.KeyToPath{
											{Key: dhParamsKey, Path: "dhparam.pem"},
										},
									},
								},
							},
						},
					},
				})
			}
			return nil
		},
	}
}

func newProxyDeployments(ch *clickHouseCluster,
	cfgChecksum string) []*KubernetesResource {
	labels := map[string]string{
		"app":  "clickhouse-proxy",
		"name": ch.Name,
	}
	annots := map[string]string{
		"checksum/config": cfgChecksum,
	}

	return []*KubernetesResource{
		newProxyDeployment(ch.Namespace, ch.Name, ch.Spec, labels, annots),
	}
}

const (
	defaultCHProxyImage = "contentsquareplatform/chproxy:1.15.0"
)

func newProxyDeployment(namespace, clusterName string, spec clickhousev1alpha1.ClickHouseSpec,
	labels, annots map[string]string) *KubernetesResource {
	name := fmt.Sprintf("%s-proxy", clusterName)
	d := &appsv1.Deployment{
		ObjectMeta: metav1.ObjectMeta{
			Name:      name,
			Namespace: namespace,
			Labels:    labels,
		},
	}

	var port = 80
	addr := strings.Split(spec.Proxy.Listeners.HTTP.Address, ":")
	if len(addr) > 1 {
		if i, err := strconv.ParseInt(addr[1], 10, 0); err == nil {
			port = int(i)
		}
	}

	return &KubernetesResource{
		obj: d,
		mutator: func() error {
			var replicas int32 = 1
			if spec.Proxy.Replicas != nil {
				replicas = *spec.Proxy.Replicas
			}
			d.Spec.Replicas = &replicas

			d.Spec.Selector = &metav1.LabelSelector{
				MatchLabels: labels,
			}
			d.Spec.Template.Labels = labels
			d.Spec.Template.Annotations = annots
			d.Spec.Template.Spec.Containers = []corev1.Container{
				{
					Name:            "chproxy",
					Image:           defaultCHProxyImage,
					ImagePullPolicy: corev1.PullIfNotPresent,
					Args: []string{
						"-config",
						"/etc/chproxy/config.yaml",
					},
					Ports: []corev1.ContainerPort{
						{
							Name:          "http",
							ContainerPort: int32(port),
						},
					},
					ReadinessProbe: &corev1.Probe{
						ProbeHandler: corev1.ProbeHandler{
							HTTPGet: &corev1.HTTPGetAction{
								Path:   "/metrics",
								Port:   intstr.FromInt(port),
								Scheme: corev1.URISchemeHTTP,
							},
						},
					},
					LivenessProbe: &corev1.Probe{
						ProbeHandler: corev1.ProbeHandler{
							HTTPGet: &corev1.HTTPGetAction{
								Path:   "/metrics",
								Port:   intstr.FromInt(port),
								Scheme: corev1.URISchemeHTTP,
							},
						},
					},
					VolumeMounts: []corev1.VolumeMount{
						{
							Name:      "chproxy-config",
							MountPath: "/etc/chproxy",
						},
					},
				},
			}
			d.Spec.Template.Spec.TerminationGracePeriodSeconds = pointer.Int64Ptr(30)
			d.Spec.Template.Spec.Volumes = []corev1.Volume{
				{
					Name: "chproxy-config",
					VolumeSource: corev1.VolumeSource{
						Secret: &corev1.SecretVolumeSource{
							SecretName: fmt.Sprintf("%s-proxy-config", clusterName),
							Items: []corev1.KeyToPath{
								{
									Key:  "config.yaml",
									Path: "config.yaml",
								},
							},
						},
					},
				},
			}

			return nil
		},
	}
}

func newCACertificateResources(ch *clickHouseCluster) []*KubernetesResource {
	kr := make([]*KubernetesResource, 0)

	issuer := issuer(ch)
	kr = append(kr, &KubernetesResource{
		obj: issuer,
		mutator: func() error {
			issuerMutator(ch, issuer)
			return nil
		},
	})

	caCert := caCertificate(ch)
	kr = append(kr, &KubernetesResource{
		obj: caCert,
		mutator: func() error {
			caCertificateMutator(ch, caCert)
			return nil
		},
	})

	return kr
}

func newCertificateResources(ch *clickHouseCluster) []*KubernetesResource {
	kr := make([]*KubernetesResource, 0)

	caIssuer := caIssuer(ch)
	kr = append(kr, &KubernetesResource{
		obj: caIssuer,
		mutator: func() error {
			caIssuerMutator(ch, caIssuer)
			return nil
		},
	})

	cert := certificate(ch)
	kr = append(kr, &KubernetesResource{
		obj: cert,
		mutator: func() error {
			certificateMutator(ch, cert)
			return nil
		},
	})

	return kr
}

func newDHParamSecret(ch *clickHouseCluster) []*KubernetesResource {
	kr := make([]*KubernetesResource, 0)

	labels := map[string]string{
		"app":  "clickhouse",
		"name": ch.Name,
	}

	kr = append(kr, newSecret(
		ch.Namespace,
		fmt.Sprintf("%s-dhparam", ch.Name),
		labels,
		map[string]string{
			dhParamsKey: ch.DHParams,
		},
	))

	return kr
}
