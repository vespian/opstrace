package controllers

import (
	certmanagerv1 "github.com/cert-manager/cert-manager/pkg/apis/certmanager/v1"
	cmmeta "github.com/cert-manager/cert-manager/pkg/apis/meta/v1"
	v1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

const (
	caCertName   = "selfsigned-ca"
	caSecretName = "root-secret"
)

func caCertificate(ch *clickHouseCluster) *certmanagerv1.Certificate {
	return &certmanagerv1.Certificate{
		ObjectMeta: v1.ObjectMeta{
			Name:      caCertName,
			Namespace: ch.Namespace,
		},
		Spec: certmanagerv1.CertificateSpec{
			IsCA:       true,
			CommonName: caCertName,
			SecretName: caSecretName,
			PrivateKey: &certmanagerv1.CertificatePrivateKey{
				Algorithm: "ECDSA",
				Size:      256,
			},
			DNSNames: []string{
				// No DNS names yet because the certificate is only for the CA authority
				// fmt.Sprintf("%s.%s.svc.cluster.local", getServiceName(), cr.Namespace()),
			},
			IssuerRef: cmmeta.ObjectReference{
				Kind: "ClusterIssuer",
				Name: rootIssuerName,
				//Group: "",
			},
			Subject: &certmanagerv1.X509Subject{
				OrganizationalUnits: []string{"clickhouse-operator"},
			},
		},
	}
}

func caCertificateMutator(ch *clickHouseCluster, current *certmanagerv1.Certificate) {
	cert := caCertificate(ch)
	current.Spec.IsCA = cert.Spec.IsCA
	current.Spec.CommonName = cert.Spec.CommonName
	current.Spec.SecretName = cert.Spec.SecretName
	current.Spec.PrivateKey = cert.Spec.PrivateKey
	current.Spec.DNSNames = cert.Spec.DNSNames
	current.Spec.IssuerRef = cert.Spec.IssuerRef
	current.Spec.Subject = cert.Spec.Subject
}
