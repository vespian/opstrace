#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

set -o pipefail

# check if docker-images.json has the required docker images set
GATEKEEPER_IMAGE=$(cd ${DIR}/../gatekeeper/ && make -s print-docker-image-name-tag)
TRACING_API_PROXY_IMAGE=$(cd ${DIR}/../go/ && make -s DOCKER_IMAGE_NAME=tracing-api print-docker-image-name-tag)
ERRORTRACKING_API_IMAGE=$(cd ${DIR}/../go/ && make -s DOCKER_IMAGE_NAME=errortracking-api print-docker-image-name-tag)
TENANT_OPERATOR_IMAGE=$(cd ${DIR}/../tenant-operator/ && make -s print-docker-image-name-tag)
CLICKHOUSE_OPERATOR_IMAGE=$(cd ${DIR}/../clickhouse-operator/ && make -s print-docker-image-name-tag)

# wrapper function to edit json file inplace
jqi() {
	cat <<< "$(jq "$1" < "$2")" > "$2"
}

# update docker-images.json
DOCKER_IMAGES_JSON=${DIR}/../go/pkg/constants/docker-images.json

jqi '.otel = "'${TRACING_API_PROXY_IMAGE}'"' ${DOCKER_IMAGES_JSON}
jqi '.gatekeeper = "'${GATEKEEPER_IMAGE}'"' ${DOCKER_IMAGES_JSON}
jqi '.tenantOperator = "'${TENANT_OPERATOR_IMAGE}'"' ${DOCKER_IMAGES_JSON}
jqi '.errorTrackingAPI = "'${ERRORTRACKING_API_IMAGE}'"' ${DOCKER_IMAGES_JSON}
jqi '.clickHouseOperator = "'${CLICKHOUSE_OPERATOR_IMAGE}'"' ${DOCKER_IMAGES_JSON}

echo "Updated docker images in go/pkg/constants, this is the diff:"
git --no-pager diff "${DOCKER_IMAGES_JSON}"

echo "Please add a commit with the required changes to docker-images.json."

# commit the latest scheduler image so we can use it to deploy the scheduler without having to always build
# and push images
echo "Updating scheduler image on scheduler/config/manager/kustomization.yaml"
$(cd ${DIR}/../scheduler/ && bin make -s update-image-tag)

echo "Updated scheduler image, this is the diff:"
git --no-pager diff "${DIR}/../scheduler/config/manager/kustomization.yaml"
echo "Please add a commit with the required changes to kustomization.yaml."
