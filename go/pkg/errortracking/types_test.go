package errortracking_test

import (
	"io/ioutil"
	"testing"

	"github.com/stretchr/testify/assert"

	"github.com/opstrace/opstrace/go/pkg/errortracking"
)

func TestNewEnvelopeFrom(t *testing.T) {
	for _, tc := range []struct {
		name     string
		filename string
	}{
		{
			name:     "should parse gitlab rails spec tests envelope",
			filename: "testdata/event.txt",
		},
		{
			name:     "should parse javascript envelope",
			filename: "testdata/javascript-sdk.txt",
		},
	} {
		t.Log(tc.name)
		payload, err := ioutil.ReadFile(tc.filename)
		assert.Nil(t, err, "failed to load test data")

		e, err := errortracking.NewEnvelopeFrom(payload)
		assert.Nil(t, err, "failed to parse test envelope")

		assert.NotPanics(t, func() {
			errortracking.NewErrorTrackingErrorEvent(uint64(1), e.Event, payload)
		}, "failed to convert error event")
	}
}

func TestNewEventFrom(t *testing.T) {
	for _, tc := range []struct {
		name     string
		filename string
	}{
		{
			name:     "should parse gitlab rails spec tests",
			filename: "testdata/ruby.1.json",
		},
		{
			name:     "should parse sentry ruby client payload",
			filename: "testdata/ruby.2.json",
		},
		{
			name:     "should parse sentry-go example error",
			filename: "testdata/go-sdk.json",
		},
	} {
		t.Logf(tc.name)
		payload, err := ioutil.ReadFile(tc.filename)
		assert.Nil(t, err, "failed to load test data")

		e, err := errortracking.NewEventFrom(payload)
		assert.Nil(t, err, "failed to parse event")

		assert.NotPanics(t, func() {
			errortracking.NewErrorTrackingErrorEvent(uint64(1), e, payload)
		}, "failed to convert error event")
	}
}
