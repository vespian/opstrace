CREATE MATERIALIZED VIEW IF NOT EXISTS error_tracking_errors_mv ON CLUSTER '{cluster}' TO $DATABASE.error_tracking_errors
AS
SELECT
    project_id,
    fingerprint,
    any(name) AS name,
    any(description) AS description,
    any(actor) AS actor,
    count() AS event_count,
    uniqState(user_identifier) AS approximated_user_count,
    max(occurred_at) AS last_seen_at,
    min(occurred_at) AS first_seen_at
FROM $DATABASE.error_tracking_error_events
GROUP BY
    project_id,
    fingerprint;
