CREATE TABLE IF NOT EXISTS error_tracking_error_status ON CLUSTER '{cluster}'
(
  project_id UInt64,
  fingerprint UInt64,
  status UInt8, -- 0 unresolved, 1 resolved
  user_id UInt64,
  actor UInt8, -- 0 status changed by user, 1 status changed by system (new event happened after resolve)
  updated_at DateTime('UTC')
) ENGINE = ReplicatedReplacingMergeTree(updated_at)
ORDER BY (project_id, fingerprint);
