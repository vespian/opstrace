// Code generated by go-swagger; DO NOT EDIT.

package errors

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"net/http"

	"github.com/go-openapi/errors"
	"github.com/go-openapi/runtime/middleware"
	"github.com/go-openapi/strfmt"
	"github.com/go-openapi/swag"
)

// NewGetErrorParams creates a new GetErrorParams object
//
// There are no default values defined in the spec.
func NewGetErrorParams() GetErrorParams {

	return GetErrorParams{}
}

// GetErrorParams contains all the bound params for the get error operation
// typically these are obtained from a http.Request
//
// swagger:parameters getError
type GetErrorParams struct {

	// HTTP Request Object
	HTTPRequest *http.Request `json:"-"`

	/*ID of the error that needs to be updated deleted
	  Required: true
	  In: path
	*/
	Fingerprint uint64
	/*ID of the project where the error was created
	  Required: true
	  In: path
	*/
	ProjectID uint64
}

// BindRequest both binds and validates a request, it assumes that complex things implement a Validatable(strfmt.Registry) error interface
// for simple values it will use straight method calls.
//
// To ensure default values, the struct must have been initialized with NewGetErrorParams() beforehand.
func (o *GetErrorParams) BindRequest(r *http.Request, route *middleware.MatchedRoute) error {
	var res []error

	o.HTTPRequest = r

	rFingerprint, rhkFingerprint, _ := route.Params.GetOK("fingerprint")
	if err := o.bindFingerprint(rFingerprint, rhkFingerprint, route.Formats); err != nil {
		res = append(res, err)
	}

	rProjectID, rhkProjectID, _ := route.Params.GetOK("projectId")
	if err := o.bindProjectID(rProjectID, rhkProjectID, route.Formats); err != nil {
		res = append(res, err)
	}
	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}

// bindFingerprint binds and validates parameter Fingerprint from path.
func (o *GetErrorParams) bindFingerprint(rawData []string, hasKey bool, formats strfmt.Registry) error {
	var raw string
	if len(rawData) > 0 {
		raw = rawData[len(rawData)-1]
	}

	// Required: true
	// Parameter is provided by construction from the route

	value, err := swag.ConvertUint64(raw)
	if err != nil {
		return errors.InvalidType("fingerprint", "path", "uint64", raw)
	}
	o.Fingerprint = value

	return nil
}

// bindProjectID binds and validates parameter ProjectID from path.
func (o *GetErrorParams) bindProjectID(rawData []string, hasKey bool, formats strfmt.Registry) error {
	var raw string
	if len(rawData) > 0 {
		raw = rawData[len(rawData)-1]
	}

	// Required: true
	// Parameter is provided by construction from the route

	value, err := swag.ConvertUint64(raw)
	if err != nil {
		return errors.InvalidType("projectId", "path", "uint64", raw)
	}
	o.ProjectID = value

	return nil
}
