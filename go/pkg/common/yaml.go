package common

import (
	"bytes"
	"html/template"
	"strings"

	"k8s.io/client-go/kubernetes/scheme"
	"sigs.k8s.io/controller-runtime/pkg/client"
)

// Parses a file with multiple yaml declarations and decodes the declarations into kubernetes client.Objects.
func ParseYaml(yamlString string) ([]client.Object, error) {
	sepYamlfiles := strings.Split(yamlString, "---")
	retVal := make([]client.Object, 0, len(sepYamlfiles))

	for _, f := range sepYamlfiles {
		if f == "\n" || f == "" {
			// ignore empty cases
			continue
		}
		decode := scheme.Codecs.UniversalDeserializer().Decode
		obj, _, err := decode([]byte(f), nil, nil)

		if err != nil {
			return retVal, err
		}
		retVal = append(retVal, obj.(client.Object))
	}
	return retVal, nil
}

// Helper to render a go template.
func RenderTemplate(t *template.Template, tmpl string, values map[string]interface{}) (string, error) {
	t, err := t.Parse(tmpl)
	if err != nil {
		return "", err
	}
	var b bytes.Buffer
	err = t.Execute(&b, values)
	if err != nil {
		return "", err
	}

	return b.String(), nil
}
