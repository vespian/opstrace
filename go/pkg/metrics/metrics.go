package metrics

import (
	"net/http"
	"strconv"
	"strings"
	"time"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
)

const (
	requestName string = "http_requests_total"
	latencyName string = "http_requests_duration_seconds"
)

var (
	requestCounter = promauto.NewCounterVec(
		prometheus.CounterOpts{
			Name: requestName,
			Help: "number of requests by status code, method and path",
		},
		[]string{"code", "method", "path"},
	)
	latencyHist = promauto.NewHistogramVec(
		prometheus.HistogramOpts{
			Name:    latencyName,
			Help:    "duration of HTTP requests by status code, method and path",
			Buckets: []float64{0.1, 0.25, 0.5, 1.0, 5.0},
		},
		[]string{"code", "method", "path"},
	)
)

func PrometheusMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		// delegate to capture response on the way back
		delegate := &responseWriterDelegator{ResponseWriter: w}
		rw := delegate

		begin := time.Now()
		next.ServeHTTP(rw, r)

		code := sanitizeCode(delegate.status)
		method := sanitizeMethod(r.Method)

		requestCounter.WithLabelValues(code, method, r.URL.Path).Inc()
		latencyHist.WithLabelValues(code, method, r.URL.Path).Observe(float64(time.Since(begin)) / float64(time.Second))
	})
}

type responseWriterDelegator struct {
	http.ResponseWriter
	status      int
	written     int64
	wroteHeader bool
}

func (r *responseWriterDelegator) WriteHeader(code int) {
	r.status = code
	r.wroteHeader = true
	r.ResponseWriter.WriteHeader(code)
}

func (r *responseWriterDelegator) Write(b []byte) (int, error) {
	if !r.wroteHeader {
		r.WriteHeader(http.StatusOK)
	}
	n, err := r.ResponseWriter.Write(b)
	r.written += int64(n)
	return n, err
}

func sanitizeMethod(m string) string {
	return strings.ToUpper(m)
}

func sanitizeCode(s int) string {
	return strconv.Itoa(s)
}
