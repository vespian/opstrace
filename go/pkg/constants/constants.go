package constants

import (
	_ "embed"
	"encoding/json"
	"fmt"
	"time"
)

//go:embed docker-images.json
var images []byte

type ImagesFromJSON struct {
	OtelImage                      string `json:"otel"`
	GatekeeperImage                string `json:"gatekeeper"`
	ErrorTrackingAPIImage          string `json:"errorTrackingAPI"`
	TenantOperator                 string `json:"tenantOperator"`
	ArgusPluginsInitContainerImage string `json:"argusPluginsInit"`
	ArgusImage                     string `json:"argus"`
	JaegerAllInOneImage            string `json:"jaegerAllInOne"`
	JaegerClickHouseImage          string `json:"jaegerClickHouse"`
	JaegerOperatorImage            string `json:"jaegerOperator"`
	ClickHouseOperatorImage        string `json:"clickHouseOperator"`
	ClickHouseImage                string `json:"clickHouse"`
	CertManagerImage               string `json:"certManager"`
	CainjectorImage                string `json:"cainjector"`
	PrometheusImage                string `json:"prometheus"`
	PrometheusOperatorImage        string `json:"prometheusOperator"`
	PrometheusConfigReloaderImage  string `json:"prometheusConfigReloader"`
	RedisOperatorImage             string `json:"redisOperator"`
	RedisMetricsExporterImage      string `json:"redisMetricsExporter"`
	ExternalDNSImage               string `json:"externalDNS"`
	NginxIngressImage              string `json:"nginxIngress"`
	KubeRBACProxyImage             string `json:"kubeRBACProxy"`
	KubeStateMetricsImage          string `json:"kubeStateMetrics"`
	AddonResizerImage              string `json:"addonResizer"`
	NodeExporterImage              string `json:"nodeExporter"`
}

func OpstraceImages() ImagesFromJSON {
	var opstraceImages = ImagesFromJSON{}
	err := json.Unmarshal(images, &opstraceImages)
	if err != nil {
		panic(fmt.Sprintf("unable to unmarshal embedded images.json: %s", err))
	}

	return opstraceImages
}

// Controller.
const (
	RequeueDelay                = time.Second * 1
	SelectorLabelName           = "app"
	HTTPSCertSecretName         = "https-cert" // #nosec G101
	TenantLabelIdentifier       = "opstrace.com/tenant"
	StorageClassName            = "pd-ssd"
	SelfSignedIssuer            = "selfsigned-issuer"
	LetsEncryptProd             = "letsencrypt-prod"
	LetsEncryptStaging          = "letsencrypt-staging"
	IngressControllerName       = "k8s.io/ingress-nginx"
	IngressClassName            = "nginx"
	PostgresCredentialNamespace = "kube-system" // legacy placement, we should move to same namespace as cluster
	PostgresCredentialName      = "postgres-secret"
	PostgresEndpointKey         = "endpoint"
)

// Clickhouse.
const (
	ClickHouseOperatorName                   = "clickhouse-operator"
	ClickHouseOperatorCredentialsSecretName  = "clickhouse-operator-credentials" // #nosec G101
	ClickHouseOperatorUsername               = "clickhouse_operator"
	ClickHouseClusterServiceName             = "cluster"
	ClickHouseServiceMonitorName             = "clickhouse"
	ClickHouseSchedulerUsername              = "opstrace_scheduler"
	ClickHouseSchedulerCredentialsSecretName = "opstrace-scheduler-clickhouse-credentials" // #nosec G101
	ClickHouseCredentialsUserKey             = "user"
	ClickHouseCredentialsPasswordKey         = "password" // #nosec G101
	ClickHouseCredentialsHTTPEndpointKey     = "http-endpoint"
	ClickHouseCredentialsNativeEndpointKey   = "native-endpoint"
)

// Argus.
const (
	ArgusDefaultClientTimeoutSeconds      = 5
	ArgusSecretsMountDir                  = "/etc/argus-secrets/" // #nosec G101
	ArgusConfigMapsMountDir               = "/etc/argus-configmaps/"
	ArgusConfigDashboardsSynced           = "argus.dashboards.synced"
	ArgusJsonnetBasePath                  = "/opt/jsonnet"
	ArgusDataPath                         = "/var/lib/argus"
	ArgusLogsPath                         = "/var/log/argus"
	ArgusPluginsPath                      = "/var/lib/argus/plugins"
	ArgusProvisioningPath                 = "/etc/argus/provisioning/"
	ArgusProvisioningPluginsPath          = "/etc/argus/provisioning/plugins"
	ArgusProvisioningDashboardsPath       = "/etc/argus/provisioning/dashboards"
	ArgusProvisioningNotifiersPath        = "/etc/argus/provisioning/notifiers"
	ArgusPluginsURL                       = "https://grafana.com/api/plugins/%s/versions/%s"
	ArgusServiceAccountName               = "argus"
	ArgusServiceName                      = "argus"
	ArgusDataStorageName                  = "argus-pvc"
	ArgusConfigName                       = "argus-config"
	ArgusConfigFileName                   = "grafana.ini"
	ArgusIngressName                      = "argus-ingress"
	ArgusDeploymentName                   = "argus-deployment"
	ArgusPluginsVolumeName                = "argus-plugins"
	ArgusInitContainerName                = "argus-plugins-init"
	ArgusProvisionPluginVolumeName        = "argus-provision-plugins"
	ArgusProvisionDashboardVolumeName     = "argus-provision-dashboards"
	ArgusProvisionNotifierVolumeName      = "argus-provision-notifiers"
	ArgusLogsVolumeName                   = "argus-logs"
	ArgusDataVolumeName                   = "argus-data"
	DatasourcesConfigMapName              = "argus-datasources"
	ArgusHealthEndpoint                   = "/api/health"
	ArgusPodLabel                         = "argus"
	LastConfigAnnotation                  = "last-config"
	LastConfigEnvVar                      = "LAST_CONFIG"
	LastDatasourcesConfigEnvVar           = "LAST_DATASOURCES"
	ArgusAdminSecretName                  = "argus-admin-credentials"    // #nosec G101
	ArgusPostgresSecretName               = "argus-postgres-credentials" // #nosec G101
	ArgusPostgresURLKey                   = "GF_DATABASE_URL"
	DefaultAdminUser                      = "admin"
	ArgusAdminUserEnvVar                  = "GF_SECURITY_ADMIN_USER"
	ArgusAdminPasswordEnvVar              = "GF_SECURITY_ADMIN_PASSWORD" // #nosec G101
	ArgusHTTPPort                     int = 3000
	ArgusHTTPPortName                     = "argus"
	ArgusSuccessMsg                       = "success"
	InstalledDashboardsDataKey            = "installed_dashboards.json"
	InstalledDashboardsConfigMapName      = "installed-dashboards"
)

// Jaeger.
const (
	JaegerOperatorName            = "jaeger-operator"
	JaegerPluginConfigPrefix      = "jaeger-plugin-config"
	JaegerNamePrefix              = "jaeger"
	JaegerOperatorServiceCertName = "jaeger-operator-service-cert"
	JaegerClickhouseSecretName    = "jaeger-clickhouse-credentials" // #nosec G101
	JaegerDatabaseName            = "opstrace_tracing"
)

// Otel.
const (
	OtelIngressPortName        = "otlp-grpc"
	OtelDeploymentNamePrefix   = "opentelemetry"
	OtelDeploymentConfigPrefix = "opentelemetry-config"
	OtelPodLabel               = "opentelemetry"
)

// CertManager.
const (
	CertManagerName = "certmanager"
	CainjectorName  = "cainjector"
)

// Prometheus.
const (
	PrometheusOperatorName = "prometheus-operator"
)

// Redis.
const (
	RedisOperatorName = "redis-operator"
	RedisName         = "redis"
)

// ExternalDNS.
const (
	ExternalDNSName = "external-dns"
)

// Gatekeeper.
const (
	GatekeeperName          = "gatekeeper"
	SessionCookieSecretName = "session-cookie-secret" // #nosec G101
)

// NginxIngress.
const (
	NginxIngressName = "nginx-ingress"
)

// TenantOperator.
const (
	TenantOperatorName = "tenant-operator"
	TenantName         = "opstrace-tenant"
)

// Error Tracking API.
const (
	ErrorTrackingAPIDomain       = "errortracking"
	ErrorTrackingAPIName         = "errortracking-api"
	ErrorTrackingAPIDatabaseName = "errortracking_api"
)
