# Mounting extra files into the Argus Pod

Sometimes Argus needs to access extra configuration files. One example is LDAP authentication where the main configuration is stored in a separate file.

The Tenant operator supports mounting of Secrets and ConfigMaps into the Argus Pod.

## Mounting Secrets and ConfigMaps

Consider the following config map containing LDAP configuration:

```yaml
apiVersion: v1
kind: ConfigMap
metadata:
  name: ldap-config
data:
  ldap.toml: |
    [[servers]]
    host = "127.0.0.1"
    port = 389
    use_ssl = false
    start_tls = false
    ssl_skip_verify = false
    bind_dn = "cn=admin,dc=grafana,dc=org"
    bind_password = 'grafana'
    search_filter = "(cn=%s)"
    search_base_dns = ["dc=grafana,dc=org"]
    [servers.attributes]
    name = "givenName"
    surname = "sn"
    username = "cn"
    member_of = "memberOf"
    email =  "email"
```

The goal is to make `ldap.toml` available inside the Pod. Use the `spec.argus.configMaps` property of the Tenant CR to automatically add a `Volume` and a `VolumeMount` to the Argus Pod:

```yaml
apiVersion: opstrace.com/v1alpha1
kind: Tenant
metadata:
  name: example-tenant
spec:
  argus:
    configMaps:
      - ldap-config
    secrets: ...
```

When mounting Secrets or ConfigMaps in Kubernetes, the keys become the file names and the values the contents of the file.
For every config map specified in this way the Tenant operator will create a volume with the name `configmap-<name>` (the prefix will be `secret-` for Secrets) and add it to the Argus deployment.
It will also create a volume mount with the same name and add it to all containers in the deployment. This includes the Argus container and all extra containers specified via the `spec.containers` property.
Config maps are mounted inside the containers under `/etc/argus-configmaps/<configmap name>/`, secrets under `/etc/argus-secrets/<secret name>/`.

The missing piece for the LDAP example is to tell Argus about the location of the configuration file. This can be done in the config section of the Argus CR:

```yaml
apiVersion: opstrace.com/v1alpha1
kind: Tenant
metadata:
  name: example-tenant
spec:
  argus:
    configMaps:
      - ldap-config
    config:
      auth.ldap:
        enabled: true
        config_file: /etc/argus-configmaps/ldap-config/ldap.toml
```

The full example can be found under `deploy/examples/ldap`.

_NOTE_: The Argus Pod will not be able to start until all specified Secrets and Config maps exist.
