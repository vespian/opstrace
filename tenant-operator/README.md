# Tenant Operator

An operator to provision and manage Opstrace Tenants, including a Grafana Instance, Dashboards, Datasources and Plugins.

This Operator began life as a fork of [`grafana-operator`](https://github.com/grafana-operator/grafana-operator) and has since been signifantly altered to fit the Opstrace usecase.

## Supported Custom Resources

The following Opstrace resources are supported:

* [`Argus`](./api/v1alpha1/tenant_types.go) - GitLab Obervability UI
* [`Dashboard`](./api/v1alpha1/dashboard_types.go)
* [`Datasource`](./api/v1alpha1/datasource_types.go)
* [`Group`](./api/v1alpha1/group_types.go)
* [`Tenant`](./api/v1alpha1/tenant_types.go)

All custom resources use the api group `opstrace.com` and version `v1alpha1`.
To get a overview of the available tenant-operator CRD see [API docs](./documentation/api.md).

## Development and Local Deployment

### Using the Makefile

If you want to develop/build/test the operator, here are some instructions how to set up your dev-environment: [follow me](./documentation/develop.md)

## Debug

We have documented a few steps to help you debug the [tenant-operator](documentation/debug.md).
