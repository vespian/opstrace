package argus

import (
	utils "github.com/opstrace/opstrace/go/pkg/common"
	"github.com/opstrace/opstrace/go/pkg/constants"
	"github.com/opstrace/opstrace/tenant-operator/api/v1alpha1"
	v1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/util/intstr"
	"sigs.k8s.io/controller-runtime/pkg/client"
)

func getServiceName() string {
	return constants.ArgusServiceName
}

func getServiceLabels() map[string]string {
	return map[string]string{
		"app": constants.ArgusPodLabel,
	}
}

func getServiceAnnotations(cr *v1alpha1.Tenant, existing map[string]string) map[string]string {
	return existing
}

func GetArgusPort(cr *v1alpha1.Tenant) int {
	return constants.ArgusHTTPPort
}

func getServicePorts(cr *v1alpha1.Tenant) []v1.ServicePort {
	return []v1.ServicePort{
		{
			Name:       constants.ArgusHTTPPortName,
			Protocol:   "TCP",
			Port:       int32(GetArgusPort(cr)),
			TargetPort: intstr.FromString("argus-http"),
		},
	}
}

func getServiceSpec(cr *v1alpha1.Tenant) v1.ServiceSpec {
	return v1.ServiceSpec{
		Ports: getServicePorts(cr),
		Selector: map[string]string{
			"app": constants.ArgusPodLabel,
		},
		Type: v1.ServiceTypeClusterIP,
	}
}

func Service(cr *v1alpha1.Tenant) *v1.Service {
	return &v1.Service{
		ObjectMeta: metav1.ObjectMeta{
			Name:        getServiceName(),
			Namespace:   cr.Namespace,
			Labels:      getServiceLabels(),
			Annotations: getServiceAnnotations(cr, nil),
		},
		Spec: getServiceSpec(cr),
	}
}

func ServiceMutator(cr *v1alpha1.Tenant, current *v1.Service) error {
	currentSpec := &current.Spec
	spec := getServiceSpec(cr)
	// Apply default overrides
	if err := utils.PatchObject(
		currentSpec,
		&spec,
	); err != nil {
		return err
	}
	// Apply CR overrides
	if err := utils.PatchObject(
		currentSpec,
		cr.Spec.Overrides.Argus.Components.Service.Spec,
	); err != nil {
		return err
	}
	current.Spec = *currentSpec
	current.Annotations = utils.MergeMap(
		getServiceAnnotations(cr, current.Annotations),
		cr.Spec.Overrides.Argus.Components.Service.Annotations,
	)
	current.Labels = utils.MergeMap(
		getServiceLabels(),
		cr.Spec.Overrides.Argus.Components.Service.Labels,
	)

	return nil
}

func ServiceSelector(cr *v1alpha1.Tenant) client.ObjectKey {
	return client.ObjectKey{
		Namespace: cr.Namespace,
		Name:      getServiceName(),
	}
}
