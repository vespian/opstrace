/*
Copyright 2021.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package group

import (
	"context"
	"errors"
	"fmt"
	"reflect"

	"github.com/go-logr/logr"
	"github.com/opstrace/opstrace/go/pkg/argusapi"
	"github.com/opstrace/opstrace/go/pkg/common"
	"github.com/opstrace/opstrace/go/pkg/constants"
	opstracev1alpha1 "github.com/opstrace/opstrace/tenant-operator/api/v1alpha1"

	ctrl "sigs.k8s.io/controller-runtime"

	"net/http"
	"net/url"

	jaegerv1 "github.com/jaegertracing/jaeger-operator/apis/v1"
	"github.com/opstrace/opstrace/tenant-operator/controllers/config"
	monitoring "github.com/prometheus-operator/prometheus-operator/pkg/apis/monitoring/v1"
	appsv1 "k8s.io/api/apps/v1"
	v1 "k8s.io/api/core/v1"
	netv1 "k8s.io/api/networking/v1"
	k8serrors "k8s.io/apimachinery/pkg/api/errors"
	apimeta "k8s.io/apimachinery/pkg/api/meta"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/client-go/tools/record"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/controller/controllerutil"
	"sigs.k8s.io/controller-runtime/pkg/reconcile"
)

const (
	finalizerName = "group.opstrace.com/finalizer"
)

// +kubebuilder:rbac:groups=opstrace.com,resources=groups,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups=opstrace.com,resources=groups/status,verbs=get;update;patch
// +kubebuilder:rbac:groups=opstrace.com,resources=groups/finalizers,verbs=update
// +kubebuilder:rbac:groups=extensions;apps,resources=deployments;deployments/finalizers,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups="",resources=events,verbs=get;list;watch;create;patch
// +kubebuilder:rbac:groups="",resources=configmaps;secrets;serviceaccounts;services;persistentvolumeclaims,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups=networking.k8s.io,resources=ingresses,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups=monitoring.coreos.com,resources=servicemonitors,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups=jaegertracing.io,resources=jaegers,verbs=get;list;watch;create;update;patch;delete

// SetupWithManager sets up the controller with the Manager.
func (r *ReconcileGroup) SetupWithManager(mgr ctrl.Manager) error {
	return ctrl.NewControllerManagedBy(mgr).
		For(&opstracev1alpha1.Group{}).
		Owns(&appsv1.Deployment{}).
		Owns(&netv1.Ingress{}).
		Owns(&v1.ConfigMap{}).
		Owns(&v1.Service{}).
		Owns(&v1.Secret{}).
		Owns(&v1.ServiceAccount{}).
		Owns(&monitoring.ServiceMonitor{}).
		Owns(&jaegerv1.Jaeger{}).
		Complete(r)
}

var _ reconcile.Reconciler = &ReconcileGroup{}

type ClientFactory func(groupId int64) (*common.ArgusClient, error)

// ReconcileGroup reconciles a Group object
type ReconcileGroup struct {
	// This client, initialized using mgr.Client() above, is a split client
	// that reads objects from the cache and writes to the apiserver
	Client    client.Client
	Scheme    *runtime.Scheme
	Transport *http.Transport
	Context   context.Context
	Cancel    context.CancelFunc
	Recorder  record.EventRecorder
	Log       logr.Logger
	Teardown  bool
}

// Reconcile , The Controller will requeue the Request to be processed again if the returned error is non-nil or
// Result.Requeue is true, otherwise upon completion it will remove the work from the queue.
func (r *ReconcileGroup) Reconcile(ctx context.Context, request reconcile.Request) (ctrl.Result, error) {
	// Disable this for now because it can cause race condition problems on teardown where if
	// Argus becomes unavailable once during teardown, it will never become available again.
	//
	// // If Argus is not running there is no need to continue
	// if !config.Get().GetState().ArgusReady {
	// 	r.Log.Info("no argus instance available")
	// 	return reconcile.Result{RequeueAfter: constants.RequeueDelay}, nil
	// }

	group := &opstracev1alpha1.Group{}
	err := r.Client.Get(r.Context, request.NamespacedName, group)
	if err != nil {
		if k8serrors.IsNotFound(err) {
			r.Log.Info("Group has been removed from API")

			return reconcile.Result{}, nil
		}
		return reconcile.Result{}, err
	}

	cr := group.DeepCopy()

	if err := r.setDeletionFinalizer(ctx, cr); err != nil {
		return reconcile.Result{}, err
	}

	// Read current state
	currentState := NewGroupState()
	err = currentState.Read(ctx, cr, r.Client)
	if err != nil {
		r.Log.Error(err, "error reading state")
		r.manageError(cr, err, request)
		return reconcile.Result{}, err
	}
	// Get the actions required to reach the desired state
	tracingReconciler := NewTracingReconciler(r.Teardown, r.Log)
	desiredState := tracingReconciler.Reconcile(&currentState.Tracing, cr)

	// Run the actions to reach the desired state
	actionRunner := common.NewActionRunner(ctx, r.Client, r.Scheme, cr)
	err = actionRunner.RunAll(desiredState)
	if err != nil {
		r.manageError(cr, err, request)
		return reconcile.Result{RequeueAfter: constants.RequeueDelay}, nil
	}

	getClient, err := r.getClientFactory()
	if err != nil {
		r.Log.Error(err, "failed to create client factory")
		return reconcile.Result{}, err
	}
	client, err := getClient(group.Spec.ID)
	if err != nil {
		r.Log.Error(err, "failed to create client")
		return reconcile.Result{}, err
	}

	if r.Teardown {
		err := client.DeleteGroup(group.Spec.ID)
		if err != nil {
			r.manageError(cr, err, request)
			return reconcile.Result{}, err
		}
	}
	// Use FullPath as name to ensure uniqueness. Name must be unique in Argus
	err = client.CreateOrUpdateGroup(&argusapi.Org{ID: group.Spec.ID, Name: group.Spec.FullPath})
	if err != nil {
		r.manageError(cr, err, request)
		return reconcile.Result{}, err
	}

	if r.Teardown {
		return reconcile.Result{}, r.teardownFinalizer(ctx, cr)
	}

	return reconcile.Result{}, r.manageSuccess(cr, request)
}

func (r *ReconcileGroup) setDeletionFinalizer(ctx context.Context, cr *opstracev1alpha1.Group) error {
	r.Teardown = false

	// examine DeletionTimestamp to determine if object is under deletion
	if cr.ObjectMeta.DeletionTimestamp.IsZero() {
		// The object is not being deleted, so if it does not have our finalizer,
		// then lets add the finalizer and update the object. This is equivalent
		// registering our finalizer.
		if !controllerutil.ContainsFinalizer(cr, finalizerName) {
			controllerutil.AddFinalizer(cr, finalizerName)

			if err := r.Client.Update(ctx, cr); err != nil {
				return fmt.Errorf("client update finalizer: %w", err)
			}
		}
	} else {
		r.Teardown = true
	}

	return nil
}

func (r *ReconcileGroup) teardownFinalizer(ctx context.Context, cr *opstracev1alpha1.Group) error {
	if controllerutil.ContainsFinalizer(cr, finalizerName) {
		// Successfully deleted everything we care about.
		// Remove our finalizer from the list and update it
		controllerutil.RemoveFinalizer(cr, finalizerName)

		if err := r.Client.Update(ctx, cr); err != nil {
			return fmt.Errorf("client remove finalizer: %w", err)
		}
	}
	return nil
}

// Handle success case
func (r *ReconcileGroup) manageSuccess(group *opstracev1alpha1.Group, request reconcile.Request) error {
	condition := metav1.Condition{
		Status:             metav1.ConditionTrue,
		Reason:             common.ReconciliationSuccessReason,
		Message:            "All components are in ready state",
		Type:               common.ConditionTypeReady,
		ObservedGeneration: group.GetGeneration(),
	}
	apimeta.SetStatusCondition(&group.Status.Conditions, condition)

	instance := &opstracev1alpha1.Group{}
	err := r.Client.Get(r.Context, request.NamespacedName, instance)
	if err != nil {
		return err
	}

	r.Log.Info("group successfully reconciled", "groupId", group.Spec.ID)
	if !reflect.DeepEqual(group.Status, instance.Status) {
		err := r.Client.Status().Update(r.Context, group)
		if err != nil {
			// Ignore conclicts. Resource might just be outdated.
			if k8serrors.IsConflict(err) {
				return nil
			}
			return fmt.Errorf("updating group status: %w", err)
		}
	}
	return nil
}

// Handle error case: update group with error message and status
func (r *ReconcileGroup) manageError(group *opstracev1alpha1.Group, issue error, request reconcile.Request) {
	condition := metav1.Condition{
		Status:             metav1.ConditionFalse,
		Reason:             common.ReconciliationFailedReason,
		Message:            issue.Error(),
		Type:               common.ConditionTypeReady,
		ObservedGeneration: group.GetGeneration(),
	}
	apimeta.SetStatusCondition(&group.Status.Conditions, condition)

	instance := &opstracev1alpha1.Group{}
	err := r.Client.Get(r.Context, request.NamespacedName, instance)
	if err != nil {
		r.Log.Error(err, "error retrieving latest instance of group")
	}

	if !reflect.DeepEqual(group.Status, instance.Status) {
		err := r.Client.Status().Update(r.Context, group)
		if err != nil {
			// Ignore conclicts. Resource might just be outdated.
			if k8serrors.IsConflict(err) {
				return
			}
			r.Log.Error(err, "error updating group status")
		}
	}
}

// Get an authenticated grafana API client
func (r *ReconcileGroup) getClientFactory() (ClientFactory, error) {
	state := config.Get().GetState()

	u := state.AdminUrl
	if u == "" {
		return nil, errors.New("cannot get grafana admin url")
	}

	parsedUrl, err := url.Parse(u)
	if err != nil {
		return nil, err
	}
	username := parsedUrl.User.Username()
	password, _ := parsedUrl.User.Password()

	return func(groupId int64) (*common.ArgusClient, error) {
		return common.NewArgusClient(u, username, password, r.Transport, groupId)
	}, nil
}
