package jaeger

import (
	"fmt"

	"github.com/opstrace/opstrace/go/pkg/common"
	"github.com/opstrace/opstrace/tenant-operator/api/v1alpha1"
	"github.com/opstrace/opstrace/tenant-operator/controllers/config"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"sigs.k8s.io/controller-runtime/pkg/client"
)

func getDatasourceName(cr *v1alpha1.Group) string {
	return GetJaegerName(cr)
}

func getDatasourceAnnotations(cr *v1alpha1.Group, existing map[string]string) map[string]string {
	return existing
}

func getDatasourceLabels(cr *v1alpha1.Group, existing map[string]string) map[string]string {
	return existing
}

func getJaegerEndpoint(cr *v1alpha1.Group) string {
	if config.Get().GetPlatformTarget() == common.KIND {
		// Connect to internal endpoint for local testing/development because we can't
		// reliably route back to the host network to re-enter via ingress when running
		// in Kind.
		return fmt.Sprintf("%s.%s.svc.cluster.local:16686", GetJaegerName(cr), cr.Namespace)
	}
	return fmt.Sprintf("%s/v1/jaeger/%d", cr.Spec.GetHostURL(), cr.Spec.ID)
}

func Datasource(cr *v1alpha1.Group) *v1alpha1.DataSource {
	return &v1alpha1.DataSource{
		ObjectMeta: metav1.ObjectMeta{
			Name:        getDatasourceName(cr),
			Namespace:   cr.Namespace,
			Labels:      getDatasourceLabels(cr, nil),
			Annotations: getDatasourceAnnotations(cr, nil),
		},
		Spec: v1alpha1.DataSourceSpec{
			// Name of the yaml file that is mounted in provisioning directory
			Name: fmt.Sprintf("%s.yaml", getDatasourceName(cr)),
			Datasources: []v1alpha1.DataSourceFields{
				{
					Name: "Tracing",
					Type: "jaeger",
					Url:  getJaegerEndpoint(cr),
					// Make this the default datasource that is
					// loaded automatically on the explore view
					IsDefault: true,
					Access:    "proxy",
					Editable:  false,
					Version:   1,
					OrgId:     int(cr.Spec.ID),
				},
			},
		},
	}
}

func DatasourceMutator(cr *v1alpha1.Group, current *v1alpha1.DataSource) error {
	// may want to add ability to update this but leaving for now
	return nil
}

func DatasourceSelector(cr *v1alpha1.Group) client.ObjectKey {
	return client.ObjectKey{
		Namespace: cr.Namespace,
		Name:      getDatasourceName(cr),
	}
}
