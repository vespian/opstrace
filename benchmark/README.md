# Benchmark scripts

The benchmark scripts implement different load testing scenarios to benchmark
monitoring APIs, including error tracking and tracing.

The scripts are developed with [Grafana k6](https://k6.io/docs/) to generate
load on API endpoints and check their status and expected functionality.

## Error tracking

Error tracking load test is is implemented in `errortracking/script.js`.
Currently, it has only a basic test for storing error events. It expects that
the POST request to `/projects/api/{PROJECT_ID}/store` endpoint succeed with
`200` status code.

Use the following environment variables to pass parameters to tests:

- `ERROR_TRACKING_API`: Error tracking API endpoint, e.g. `https://example.com/errortracking/api/v1`.
- `PROJECT_ID`: The ID of the project to run tests against it.
- `SENTRY_SECRET`: The authorization secret which can be generated via UI. _This
  parameter is required._
- `PAYLOAD_FILE`: The base payload. Note that test scripts overwrite `event_id`
  and `timestamp` attributes of the payload to generate a unique payload for
  each request. By default it uses `errortrackning/payload/2k.json`.

Note that the default values for some of these parameters are stored in `.env`
file.

The configuration for different load test scenarios are provided in separate
files in `errortracking/config/`, including:

- `baseline.json`: 100 requests per second for 5 minutes;
- `medium.json`: 1,000 requests per second for 5 minutes;
- `high.json`: 10,000 requests per second for 5 minutes.

Use each configuration to run a specific scenario.

### Usage

Here is an example usage:

```shell
source .env # Alternatively use `direnv allow`

# Authorization secret is required
export SENTRY_SECRET='<SECRET>'

# Run baseline scenario
k6 run -c errortracking/config/baseline.json errortracking/script.js
```
