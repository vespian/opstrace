variable "project_id" {
  description = "project id"
}

variable "region" {
  description = "region"
}

variable "gke_username" {
  default     = ""
  description = "gke username"
}

variable "gke_password" {
  default     = ""
  description = "gke password"
}

variable "location" {
  default     = ""
  description = "The location (region or zone) in which the cluster master will be created, as well as the default node location. If you specify a zone (such as us-central1-a), the cluster will be a zonal cluster with a single cluster master. If you specify a region (such as us-west1), the cluster will be a regional cluster with multiple masters spread across zones in the region, and with default node locations in those zones as well"
}

variable "instance_name" {
  default     = "example"
  description = "the Opstrace instance name"
}

variable "num_nodes" {
  default     = 3
  description = "number of gke nodes"
}

variable "kubeconfig_path" {
  default     = ".kubeconfig"
  description = "Path to kubeconfig"
}