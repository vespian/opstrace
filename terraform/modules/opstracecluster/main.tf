resource "kubernetes_secret" "cluster" {
  provider = kubernetes
  metadata {
    name      = var.cluster_secret_name
    namespace = var.cluster_secret_namespace
  }

  data = {
    gitlab_oauth_client_id     = var.gitlab_oauth_client_id
    gitlab_oauth_client_secret = var.gitlab_oauth_client_secret
    internal_endpoint_token    = var.internal_endpoint_token
  }

  immutable = true
}

resource "kubectl_manifest" "cluster" {
  yaml_body = var.cluster_manifest
}
