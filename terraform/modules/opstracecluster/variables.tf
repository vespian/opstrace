variable "cluster_endpoint" {
  type        = string
  description = "Cluster endpoint"
}

variable "cluster_ca_certificate" {
  type        = string
  description = "Cluster CA certificate"
}

variable "kubeconfig_path" {
  type    = string
  default = ".kubeconfig"
}

// gitlab_oauth_client_id: string representing application client ID
variable "gitlab_oauth_client_id" {
  type      = string
  sensitive = true
  default   = "samplesecret"
}

// gitlab_oauth_client_secret: string representing application client secret
variable "gitlab_oauth_client_secret" {
  type      = string
  sensitive = true
  default   = "samplesecret"
}

// internal_endpoint_token: the token to talk to Gitlab internal auth API
variable "internal_endpoint_token" {
  type      = string
  sensitive = true
  default   = "samplesecret"
}

variable "cluster_secret_name" {
  type    = string
  default = "auth-secret"
}

variable "cluster_secret_namespace" {
  type    = string
  default = "default"
}

variable "cluster_manifest" {
  type    = string
  default = ""
}
