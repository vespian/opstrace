terraform {
  backend "local" {}

  required_providers {
    kubernetes = {
      source = "hashicorp/kubernetes"
      version = "2.11.0"
    }

    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.22.0"
    }

  }

  required_version = ">= 0.15"
}