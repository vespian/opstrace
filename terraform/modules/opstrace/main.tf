# TODO: create a secret for the docker hub credentials.
# When we do this, we'll also have to plumb all imagePullSecrets in the
# CRs (Cluster, Tenant) through to the container definitions.
# Some are plumbed through, but not all.
# https://gitlab.com/gitlab-org/opstrace/opstrace/-/issues/1717

resource "kubernetes_secret" "postgres-secret" {
  metadata {
    name      = "postgres-secret"
    namespace = "kube-system"
  }

  data = {
    endpoint = var.postgres_dsn_endpoint
  }

}

# ----------------------------------------------------------
# Opstrace - scheduler, tenant-operator, clickhouse-operator
# ----------------------------------------------------------

data "kustomization_build" "scheduler-crds" {
  path = "${path.module}/../../../scheduler/config/crd"
}

resource "kustomization_resource" "scheduler-crds" {
  for_each = data.kustomization_build.scheduler-crds.ids
  manifest = data.kustomization_build.scheduler-crds.manifests[each.value]
}

data "kustomization_build" "tenant-operator-crds" {
  path = "${path.module}/../../../tenant-operator/config/crd"
}

resource "kustomization_resource" "tenant-operator-crds" {
  for_each = data.kustomization_build.tenant-operator-crds.ids
  manifest = data.kustomization_build.tenant-operator-crds.manifests[each.value]
}

data "kustomization_build" "clickhouse-operator-crds" {
  path = "${path.module}/../../../clickhouse-operator/config/crd"
}

resource "kustomization_resource" "clickhouse-operator-crds" {
  # clickhouse-operator kustomization brings in both clickhouse & certmanager CRDs:
  # https://gitlab.com/gitlab-org/opstrace/opstrace/-/blob/main/clickhouse-operator/config/crd/kustomization.yaml
  #
  # since we already install certmanager CRDs from the scheduler kustomization (code upstream):
  # https://gitlab.com/gitlab-org/opstrace/opstrace/-/blob/main/scheduler/config/crd/kustomization.yaml
  #
  # we filter them out here explicitly. Should clean up once this is not the case
  # anymore OR we bring in certmanager CRDs completely outside of any other component.
  for_each = {
    for key, val in data.kustomization_build.clickhouse-operator-crds.ids:
      key => val if key == "apiextensions.k8s.io/CustomResourceDefinition/_/clickhouses.clickhouse.gitlab.com"
  }
  manifest = data.kustomization_build.clickhouse-operator-crds.manifests[each.value]
}

data "kustomization_build" "scheduler" {
  path = "${path.module}/../../../scheduler/config/deploy"
}

resource "kustomization_resource" "scheduler" {
  for_each = data.kustomization_build.scheduler.ids
  manifest = data.kustomization_build.scheduler.manifests[each.value]

  depends_on = [
    kustomization_resource.scheduler-crds,
    kustomization_resource.tenant-operator-crds
  ]
}
