variable "cluster_endpoint" {
  type        = string
  description = "Cluster endpoint"
}

variable "cluster_ca_certificate" {
  type        = string
  description = "Cluster CA certificate"
}

variable "kubeconfig_path" {
  type = string
  default = ".kubeconfig"
}

variable "postgres_dsn_endpoint" {
  type = string
  sensitive = true
  description = "Postgres DSN endpoint to put in a secret"
}
