terraform {
    source = "${get_terragrunt_dir()}/../../../modules//opstracecluster"
}

include {
    path = find_in_parent_folders()
}

dependency "gke" {
    config_path = "../gke"
    mock_outputs = {
        kubernetes_cluster_host = "https://mock"
        kubernetes_cluster_certificate = "mock"
        kubeconfig_path = "~/.kube/config"
        certmanager_service_account = "mock-sa"
        externaldns_service_account = "mock-sa"
    }
    mock_outputs_allowed_terraform_commands = ["validate"]
}

dependency "gitlab" {
    config_path = "../gitlab"
    mock_outputs = {
        gitlab_url = "https://gitlab.com"
        oauth_client_id = "samplesecret"
        oauth_client_secret = "samplesecret"
        internal_endpoint_token = "samplesecret"
    }
    mock_outputs_allowed_terraform_commands = ["validate"]
}

dependency "opstrace" {
    config_path = "../opstrace"
    skip_outputs = true
}

locals {
    common_vars = read_terragrunt_config(find_in_parent_folders("env.hcl"))
}

inputs = {
    cluster_endpoint = dependency.gke.outputs.kubernetes_cluster_host
    cluster_ca_certificate = dependency.gke.outputs.kubernetes_cluster_certificate
    kubeconfig_path = dependency.gke.outputs.kubeconfig_path
    cluster_secret_name = local.common_vars.inputs.cluster_secret_name
    cluster_secret_namespace = local.common_vars.inputs.cluster_secret_namespace
    gitlab_oauth_client_id = dependency.gitlab.outputs.oauth_client_id
    gitlab_oauth_client_secret = dependency.gitlab.outputs.oauth_client_secret
    internal_endpoint_token = dependency.gitlab.outputs.internal_endpoint_token
    cluster_manifest = yamlencode({
        "apiVersion": "opstrace.com/v1alpha1"
        "kind": "Cluster"
        "metadata": {
            "name": local.common_vars.inputs.instance_name,
            "namespace": "default"
        },
        "spec": {
            "target": "gcp",
            "dns": {
                "certificateIssuer": local.common_vars.inputs.cert_issuer,
                "domain": local.common_vars.inputs.opstrace_domain,
                "acmeEmail": local.common_vars.inputs.acme_email,
                "acmeServer": local.common_vars.inputs.acme_server,
                "dns01Challenge": {
                    # Use google cloud DNS (where our domain is hosted).
                    "cloudDNS": {
                        "project": local.common_vars.inputs.project_id
                    }
                },
                "externalDNSProvider": {
                    # Use google cloud DNS (where our domain is hosted).
                    "gcp": {
                        "dnsServiceAccountName": dependency.gke.outputs.externaldns_service_account
                    }
                },
                "firewallSourceIPsAllowed": ["0.0.0.0/0"]
                # Use google cloud DNS (where our domain is hosted).
                "gcpCertManagerServiceAccount": dependency.gke.outputs.certmanager_service_account
            },
            "gitlab": {
                "instanceUrl": dependency.gitlab.outputs.gitlab_url,
                "groupAllowedAccess": "*",
                "groupAllowedSystemAccess": "*",
                "authSecret": {
                    "name": local.common_vars.inputs.cluster_secret_name
                }
            },
            "overrides": {
                #
                # This is where we can specify affinities and node selectors or override any
                # aspect of any component. More nodeSelectors/overrides capabilities may need to be added to this CRD
                # for resources that are created by the tenant-operator (Argus, Jaeger CR, Opentelemetry API, etc).
                # Currently those overrides can only be specified on the Group CR, which is not tracked in terraform because
                # its an internal dynamic resource.
                #
                "gatekeeper": {
                    "components": {
                        "deployment": {
                            "spec": {
                                "template": {
                                    "spec": {
                                        "replicas": 3,
                                        "containers": [{
                                            "name": "gatekeeper"
                                        }]
                                    }
                                }
                            }
                        },
                        "ingress": {},
                        "service": {},
                        "serviceMonitor": {},
                    }
                }
            }
        }
    })
}
