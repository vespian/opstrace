terraform {
    source = "${get_terragrunt_dir()}/../../../..//terraform/modules/opstrace"
}

include {
    path = find_in_parent_folders()
}

dependency "gitlab" {
    config_path = "../gitlab"
    skip_outputs = true
}

dependency "gke" {
    config_path = "../gke"
    mock_outputs = {
        kubernetes_cluster_host = "https://mock"
        kubernetes_cluster_certificate = "mock"
        kubeconfig_path = "~/.kube/config"
        postgres_dsn_endpoint = "mock"
    }
    mock_outputs_allowed_terraform_commands = ["validate"]
}

inputs = {
    cluster_endpoint = dependency.gke.outputs.kubernetes_cluster_host
    cluster_ca_certificate = dependency.gke.outputs.kubernetes_cluster_certificate
    kubeconfig_path = dependency.gke.outputs.kubeconfig_path
    postgres_dsn_endpoint = dependency.gke.outputs.postgres_dsn_endpoint
}