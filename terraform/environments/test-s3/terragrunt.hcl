locals {}

remote_state {
    backend = "local"
    config = {
        path="${get_terragrunt_dir()}/terraform.tfstate"
    }
}