package nginxIngress

import (
	"github.com/opstrace/opstrace/go/pkg/common"
	utils "github.com/opstrace/opstrace/go/pkg/common"
	"github.com/opstrace/opstrace/scheduler/api/v1alpha1"
	v1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/util/intstr"
	"sigs.k8s.io/controller-runtime/pkg/client"
)

func getServiceName() string {
	return GetNginxIngressDeploymentName()
}

func getServiceLabels() map[string]string {
	return GetNginxIngressDeploymentSelector()
}

func getServiceAnnotations(cr *v1alpha1.Cluster, existing map[string]string) map[string]string {
	a := map[string]string{
		"external-dns.alpha.kubernetes.io/hostname": cr.Spec.GetHost(),
		"external-dns.alpha.kubernetes.io/ttl":      "30",
	}

	if cr.Spec.Target == common.AWS {
		// Use an NLB type loadbalancer
		a["service.beta.kubernetes.io/aws-load-balancer-type"] = "nlb"
		// Explicitly choose an L4 tcp loadbalancer
		a["service.beta.kubernetes.io/aws-load-balancer-backend-protocol"] = "tcp"
		//  Ensure the ELB idle timeout is less than nginx keep-alive timeout. Because we're
		//  using WebSockets, the value will need to be
		//  increased to '3600' to avoid any potential issues. We set the NGINX timeout to 3700
		//  so that it's longer than the LB timeout.
		a["service.beta.kubernetes.io/aws-load-balancer-connection-idle-timeout"] = "3600"
	}

	return utils.MergeMap(existing, a)
}

func getServicePorts() []v1.ServicePort {
	return []v1.ServicePort{
		{
			Name:       "http",
			Port:       80,
			TargetPort: intstr.FromString("http"),
		},
		{
			Name:       "https",
			Port:       443,
			TargetPort: intstr.FromString("https"),
		},
	}
}

func getServiceSpec(cr *v1alpha1.Cluster) v1.ServiceSpec {
	serviceType := v1.ServiceTypeLoadBalancer

	if cr.Spec.Target == common.KIND {
		// Create a nodeport for local dev
		serviceType = v1.ServiceTypeNodePort
	}

	return v1.ServiceSpec{
		Ports:                    getServicePorts(),
		Selector:                 GetNginxIngressDeploymentSelector(),
		Type:                     serviceType,
		LoadBalancerSourceRanges: cr.Spec.DNS.FirewallSourceIPsAllowed,
	}
}

func Service(cr *v1alpha1.Cluster) *v1.Service {
	return &v1.Service{
		ObjectMeta: metav1.ObjectMeta{
			Name:        getServiceName(),
			Namespace:   cr.Namespace(),
			Labels:      getServiceLabels(),
			Annotations: getServiceAnnotations(cr, nil),
		},
		Spec: getServiceSpec(cr),
	}
}

func ServiceMutator(cr *v1alpha1.Cluster, current *v1.Service) error {
	current.Name = getServiceName()
	currentSpec := current.Spec.DeepCopy()
	spec := getServiceSpec(cr)
	// Apply default overrides
	if err := utils.PatchObject(
		currentSpec,
		&spec,
	); err != nil {
		return err
	}
	// Apply CR overrides
	if err := utils.PatchObject(
		currentSpec,
		cr.Spec.Overrides.NginxIngress.Components.Service.Spec,
	); err != nil {
		return err
	}
	current.Spec = *currentSpec
	current.Annotations = utils.MergeMap(
		getServiceAnnotations(cr, current.Annotations),
		cr.Spec.Overrides.NginxIngress.Components.Service.Annotations,
	)
	current.Labels = utils.MergeMap(
		getServiceLabels(),
		cr.Spec.Overrides.NginxIngress.Components.Service.Labels,
	)

	return nil
}

func ServiceSelector(cr *v1alpha1.Cluster) client.ObjectKey {
	return client.ObjectKey{
		Namespace: cr.Namespace(),
		Name:      getServiceName(),
	}
}
