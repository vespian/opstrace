package nginxIngress

import (
	"github.com/opstrace/opstrace/go/pkg/constants"
	"github.com/opstrace/opstrace/scheduler/api/v1alpha1"
	netv1 "k8s.io/api/networking/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"sigs.k8s.io/controller-runtime/pkg/client"
)

func getIngressClassName() string {
	return constants.IngressClassName
}

func getIngressClassLabels() map[string]string {
	return GetNginxIngressDeploymentSelector()
}

func getIngressClassAnnotations(cr *v1alpha1.Cluster, existing map[string]string) map[string]string {
	return existing
}

func IngressClass(cr *v1alpha1.Cluster) *netv1.IngressClass {
	return &netv1.IngressClass{
		ObjectMeta: metav1.ObjectMeta{
			Name:        getIngressClassName(),
			Labels:      getIngressClassLabels(),
			Annotations: getIngressClassAnnotations(cr, nil),
		},
		Spec: netv1.IngressClassSpec{
			Controller: constants.IngressControllerName,
		},
	}
}

func IngressClassSelector() client.ObjectKey {
	return client.ObjectKey{
		Name: getIngressClassName(),
	}
}

func IngressClassMutator(cr *v1alpha1.Cluster, current *netv1.IngressClass) {
	i := IngressClass(cr)
	current.Labels = getIngressClassLabels()
	current.Annotations = getIngressClassAnnotations(cr, current.Annotations)
	current.Spec = i.Spec
}
