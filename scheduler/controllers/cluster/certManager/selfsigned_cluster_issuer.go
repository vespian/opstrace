package certManager

import (
	certmanager "github.com/cert-manager/cert-manager/pkg/apis/certmanager/v1"
	"github.com/opstrace/opstrace/go/pkg/constants"
	v1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"sigs.k8s.io/controller-runtime/pkg/client"
)

//
// Self signed Issuers will issue self signed certificates. This is useful
// when building PKI within Kubernetes, or as a means to generate a root CA
// for use with the CA Issuer. A self-signed Issuer contains no additional
// configuration fields.
//
func SelfSignedClusterIssuer() *certmanager.ClusterIssuer {
	return &certmanager.ClusterIssuer{
		ObjectMeta: v1.ObjectMeta{
			Name: constants.SelfSignedIssuer,
		},
		Spec: certmanager.IssuerSpec{
			IssuerConfig: certmanager.IssuerConfig{
				SelfSigned: &certmanager.SelfSignedIssuer{},
			},
		},
	}
}

func SelfSignedClusterIssuerMutator(current *certmanager.ClusterIssuer) {
	issuer := SelfSignedClusterIssuer()
	current.Spec.SelfSigned = issuer.Spec.SelfSigned
}

func SelfSignedClusterIssuerSelector() client.ObjectKey {
	return client.ObjectKey{
		Name: constants.SelfSignedIssuer,
	}
}
