package cluster

import (
	"fmt"

	"github.com/go-logr/logr"
	"github.com/opstrace/opstrace/go/pkg/common"
	"github.com/opstrace/opstrace/scheduler/api/v1alpha1"
	"github.com/opstrace/opstrace/scheduler/controllers/cluster/prometheus"
	"sigs.k8s.io/controller-runtime/pkg/client"
)

type PrometheusReconciler struct {
	Teardown bool
	Log      logr.Logger
}

func NewPrometheusReconciler(teardown bool, logger logr.Logger) *PrometheusReconciler {
	return &PrometheusReconciler{
		Teardown: teardown,
		Log:      logger.WithName("prometheus"),
	}
}

func (p *PrometheusReconciler) Reconcile(state *ClusterState, cr *v1alpha1.Cluster) common.DesiredState {
	desired := common.DesiredState{}

	desired = desired.AddAction(p.getServiceAccountDesiredState(cr))
	desired = desired.AddActions(p.getRBACDesiredState(cr))
	desired = desired.AddAction(p.getServiceDesiredState(cr))
	desired = desired.AddAction(p.getCRDesiredState(cr))
	desired = desired.AddAction(p.getServiceMonitorDesiredState(cr))

	desired = desired.AddActions(p.getReadiness(state))

	return desired
}

func (p *PrometheusReconciler) getReadiness(state *ClusterState) []common.Action {
	return []common.Action{}
}

func (p *PrometheusReconciler) getServiceAccountDesiredState(cr *v1alpha1.Cluster) common.Action {
	sa := prometheus.ServiceAccount(cr)

	if p.Teardown {
		return common.GenericDeleteAction{
			Ref: sa,
			Msg: "prometheus service account",
		}
	}

	return common.GenericCreateOrUpdateAction{
		Ref: sa,
		Msg: "prometheus service account",
		Mutator: func() error {
			prometheus.ServiceAccountMutator(cr, sa)
			return nil
		},
	}
}

func (p *PrometheusReconciler) getRBACDesiredState(cr *v1alpha1.Cluster) []common.Action {
	objects, err := prometheus.GetRBACObjects(cr)
	if err != nil {
		return []common.Action{common.LogAction{
			Msg:   "failed to serialize prometheus rbac resources",
			Error: err,
		}}
	}
	actions := []common.Action{}
	for _, obj := range objects {
		obj := obj
		//nolint:errcheck
		desired := obj.DeepCopyObject().(client.Object)

		if p.Teardown {
			actions = append(actions, common.GenericDeleteAction{
				Ref: obj,
				Msg: fmt.Sprintf("prometheus %s", obj.GetObjectKind().GroupVersionKind().Kind),
			})
		} else {
			actions = append(actions, common.GenericCreateOrUpdateAction{
				Ref: obj,
				Msg: fmt.Sprintf("prometheus %s", obj.GetObjectKind().GroupVersionKind().Kind),
				Mutator: func() error {
					return common.RBACObjectMutator(obj, desired)
				},
			})
		}
	}

	return actions
}

func (p *PrometheusReconciler) getServiceDesiredState(cr *v1alpha1.Cluster) common.Action {
	service := prometheus.Service(cr)

	if p.Teardown {
		return common.GenericDeleteAction{
			Ref: service,
			Msg: "prometheus service",
		}
	}

	return common.GenericCreateOrUpdateAction{
		Ref: service,
		Msg: "prometheus service",
		Mutator: func() error {
			return prometheus.ServiceMutator(cr, service)
		},
	}
}

func (p *PrometheusReconciler) getCRDesiredState(cr *v1alpha1.Cluster) common.Action {
	pr, err := prometheus.Prometheus(cr)
	if err != nil {
		return common.LogAction{
			Msg:   "failed to get prometheus CR",
			Error: err,
		}
	}

	if p.Teardown {
		return common.GenericDeleteAction{
			Ref: pr,
			Msg: "prometheus CR",
		}
	}

	return common.GenericCreateOrUpdateAction{
		Ref: pr,
		Msg: "prometheus CR",
		Mutator: func() error {
			return prometheus.PrometheusMutator(cr, pr)
		},
	}
}

func (m *PrometheusReconciler) getServiceMonitorDesiredState(cr *v1alpha1.Cluster) common.Action {
	sm := prometheus.ServiceMonitor(cr)

	if m.Teardown {
		return common.GenericDeleteAction{
			Ref: sm,
			Msg: "prometheus service monitor",
		}
	}

	return common.GenericCreateOrUpdateAction{
		Ref: sm,
		Msg: "prometheus service monitor",
		Mutator: func() error {
			return prometheus.ServiceMonitorMutator(cr, sm)
		},
	}
}
