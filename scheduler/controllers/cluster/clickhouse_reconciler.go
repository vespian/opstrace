package cluster

import (
	"errors"
	"fmt"

	"github.com/go-logr/logr"
	"github.com/opstrace/opstrace/go/pkg/common"
	"github.com/opstrace/opstrace/go/pkg/constants"
	"github.com/opstrace/opstrace/scheduler/api/v1alpha1"
	"github.com/opstrace/opstrace/scheduler/controllers/cluster/clickhouse"
	"k8s.io/apimachinery/pkg/api/equality"
)

type ClickHouseReconciler struct {
	Teardown bool
	Log      logr.Logger
}

func NewClickHouseReconciler(teardown bool, logger logr.Logger) *ClickHouseReconciler {
	return &ClickHouseReconciler{
		Teardown: teardown,
		Log:      logger.WithName("clickhouse"),
	}
}

func (i *ClickHouseReconciler) Reconcile(state *ClusterState, cr *v1alpha1.Cluster) common.DesiredState {
	desired := common.DesiredState{}

	desired = desired.AddAction(i.getCredentialsDesiredState(cr))
	desired = desired.AddAction(i.getClickHouseDesiredState(state, cr))
	desired = desired.AddAction(i.getServiceMonitorDesiredState(cr))

	desired = desired.AddActions(i.getReadiness(state))

	return desired
}

func (i *ClickHouseReconciler) getReadiness(state *ClusterState) []common.Action {
	if i.Teardown {
		return []common.Action{
			common.CheckGoneAction{
				Ref: state.ClickHouse.Cluster,
				Msg: "check clickhouse cluster is gone",
			},
		}
	}

	endpoints, err := state.ClickHouse.GetEndpoints()
	if err != nil {
		return []common.Action{common.LogAction{
			Msg:   "failed to obtain clickhouse scheduler endpoints, this should be a transient error that resolves on next reconcile",
			Error: err,
		}}
	}

	if state.ClickHouse.isClusterReady() {
		return []common.Action{common.LogAction{
			Msg: "clickhouse cluster is ready",
		},
			common.ClickHouseAction{
				Msg: fmt.Sprintf("create clickhouse database if not exists: %s", constants.JaegerDatabaseName),
				SQL: fmt.Sprintf("CREATE DATABASE IF NOT EXISTS %s ON CLUSTER '{cluster}'",
					constants.JaegerDatabaseName),
				URL: endpoints.Native,
			},
			common.ClickHouseAction{
				Msg: fmt.Sprintf("create clickhouse database if not exists: %s", constants.ErrorTrackingAPIDatabaseName),
				SQL: fmt.Sprintf("CREATE DATABASE IF NOT EXISTS %s ON CLUSTER '{cluster}'",
					constants.ErrorTrackingAPIDatabaseName),
				URL: endpoints.Native,
			},
		}
	}
	return []common.Action{common.LogAction{
		Msg:   "clickhouse cluster not ready",
		Error: errors.New("clickhouse cluster not ready"),
	}}
}

func (i *ClickHouseReconciler) getCredentialsDesiredState(cr *v1alpha1.Cluster) common.Action {
	s := clickhouse.Credentials(cr)

	if i.Teardown {
		return common.GenericDeleteAction{
			Ref: s,
			Msg: "clickhouse credentials",
		}
	}

	return common.GenericCreateOrUpdateAction{
		Ref: s,
		Msg: "clickhouse credentials",
		Mutator: func() error {
			return clickhouse.CredentialsMutator(cr, s)
		},
	}
}

func (i *ClickHouseReconciler) getClickHouseDesiredState(state *ClusterState, cr *v1alpha1.Cluster) common.Action {
	user, err := state.ClickHouse.GetSchedulerCredentials()
	if err != nil {
		return common.LogAction{
			Msg:   "failed to obtain clickhouse scheduler user credentials, this should be a transient error that resolves on next reconcile",
			Error: err,
		}
	}

	current := state.ClickHouse.Cluster

	if i.Teardown {
		return common.GenericDeleteAction{
			Ref: current,
			Msg: "clickhouse CR",
		}
	}

	if current == nil {
		return common.GenericCreateAction{
			Ref: clickhouse.ClickHouse(cr, user),
			Msg: "clickhouse CR",
		}
	}

	existing := current.DeepCopy()

	err = clickhouse.ClickHouseMutator(cr, current, user)
	if err != nil {
		return common.LogAction{
			Msg:   "failed to mutate clickhouse CR",
			Error: err,
		}
	}

	if equality.Semantic.DeepEqual(existing, current) {
		return common.LogAction{
			Msg: "clickhouse CR unchanged",
		}
	}

	return common.GenericUpdateAction{
		Ref: current,
		Msg: "clickhouse CR",
	}
}

func (i *ClickHouseReconciler) getServiceMonitorDesiredState(cr *v1alpha1.Cluster) common.Action {
	monitor := clickhouse.ServiceMonitor(cr)

	if i.Teardown {
		return common.GenericDeleteAction{
			Ref: monitor,
			Msg: "clickhouse cluster servicemonitor",
		}
	}

	return common.GenericCreateOrUpdateAction{
		Ref: monitor,
		Msg: "clickhouse cluster servicemonitor",
		Mutator: func() error {
			return clickhouse.ServiceMonitorMutator(cr, monitor)
		},
	}
}
