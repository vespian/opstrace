package kubescheduler

import (
	"embed"

	"github.com/opstrace/opstrace/scheduler/api/v1alpha1"
	monitors "github.com/opstrace/opstrace/scheduler/controllers/cluster/monitoring"
	prometheus "github.com/opstrace/opstrace/scheduler/controllers/cluster/prometheus/helpers"
	monitoring "github.com/prometheus-operator/prometheus-operator/pkg/apis/monitoring/v1"
)

//go:embed kubescheduler_rules.yaml
var f embed.FS

func PrometheusRule(cr *v1alpha1.Cluster) (*monitoring.PrometheusRule, error) {
	corpus, err := f.ReadFile("kubescheduler_rules.yaml")
	if err != nil {
		return &monitoring.PrometheusRule{}, err
	}
	return prometheus.BuildPrometheusRule(cr, monitors.KubeScheduler, corpus)
}

func PrometheusRuleMutator(cr *v1alpha1.Cluster, current *monitoring.PrometheusRule) error {
	rule, err := PrometheusRule(cr)
	if err != nil {
		return err
	}
	current.Spec = rule.Spec
	return nil
}
