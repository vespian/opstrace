package prometheus

import (
	"encoding/json"

	"github.com/opstrace/opstrace/go/pkg/common"
	"github.com/opstrace/opstrace/go/pkg/constants"
	"github.com/opstrace/opstrace/scheduler/api/v1alpha1"
	monitors "github.com/opstrace/opstrace/scheduler/controllers/cluster/monitoring"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/resource"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"sigs.k8s.io/controller-runtime/pkg/client"

	monitoring "github.com/prometheus-operator/prometheus-operator/pkg/apis/monitoring/v1"
)

var (
	storageClassName  string = "pd-ssd"
	prometheusImage   string = constants.OpstraceImages().PrometheusImage
	prometheusVersion string = "v2.25.1"
	fsGroup           int64  = 2000
	runAsNonRoot      bool   = true
	runAsUser         int64  = 1000
	replicas          int32  = 1
)

func Prometheus(cr *v1alpha1.Cluster) (*monitoring.Prometheus, error) {
	defaultSpec, err := getPrometheusSpec(cr)
	if err != nil {
		return nil, err
	}

	return &monitoring.Prometheus{
		ObjectMeta: metav1.ObjectMeta{
			Name:      monitors.Prometheus,
			Namespace: cr.Namespace(),
			Labels:    getServiceLabels(),
		},
		Spec: defaultSpec,
	}, nil
}

func PrometheusMutator(cr *v1alpha1.Cluster, current *monitoring.Prometheus) error {
	currentSpec := &current.Spec
	spec, err := getPrometheusSpec(cr)
	if err != nil {
		return err
	}

	// apply default overrides
	if err := common.PatchObject(currentSpec, &spec); err != nil {
		return err
	}

	overrides := cr.Spec.Overrides.Prometheus.PrometheusSpec
	if overrides.Raw != nil && len(overrides.Raw) > 0 {
		currentBytes, err := json.Marshal(currentSpec)
		if err != nil {
			return err
		}

		// apply CR overrides
		patched, err := common.PatchBytes(currentBytes, overrides.Raw)
		if err != nil {
			return err
		}

		// unmarshal into original spec
		if err := json.Unmarshal(patched, currentSpec); err != nil {
			return err
		}
	}

	current.Spec = *currentSpec
	return nil
}

func PrometheusSelector(cr *v1alpha1.Cluster) client.ObjectKey {
	return client.ObjectKey{
		Name:      monitors.Prometheus,
		Namespace: cr.Namespace(),
	}
}

//nolint:unparam
func getPrometheusSpec(cr *v1alpha1.Cluster) (monitoring.PrometheusSpec, error) {
	return monitoring.PrometheusSpec{
		RuleSelector: &metav1.LabelSelector{
			MatchLabels: map[string]string{
				"tenant": "system",
			},
		},
		RuleNamespaceSelector: &metav1.LabelSelector{
			MatchLabels: map[string]string{},
		},
		CommonPrometheusFields: monitoring.CommonPrometheusFields{
			ServiceMonitorSelector: &metav1.LabelSelector{
				MatchLabels: map[string]string{
					"tenant": "system",
				},
			},
			ServiceMonitorNamespaceSelector: &metav1.LabelSelector{},
			ExternalURL:                     "https://localhost/prometheus",
			RoutePrefix:                     "/prometheus",
			Resources: corev1.ResourceRequirements{
				Requests: corev1.ResourceList{
					corev1.ResourceCPU:    resource.MustParse("1000m"),
					corev1.ResourceMemory: resource.MustParse("5Gi"),
				},
				Limits: corev1.ResourceList{
					corev1.ResourceCPU:    resource.MustParse("1000m"),
					corev1.ResourceMemory: resource.MustParse("5Gi"),
				},
			},
			Storage: &monitoring.StorageSpec{
				VolumeClaimTemplate: monitoring.EmbeddedPersistentVolumeClaim{
					Spec: corev1.PersistentVolumeClaimSpec{
						StorageClassName: &storageClassName,
						AccessModes: []corev1.PersistentVolumeAccessMode{
							corev1.ReadWriteOnce,
						},
						Resources: corev1.ResourceRequirements{
							Requests: corev1.ResourceList{
								corev1.ResourceStorage: resource.MustParse("10Gi"),
							},
						},
					},
				},
			},
			Replicas: &replicas,
			Image:    &prometheusImage,
			Version:  prometheusVersion,
			NodeSelector: map[string]string{
				"kubernetes.io/os": "linux",
			},
			SecurityContext: &corev1.PodSecurityContext{
				FSGroup:      &fsGroup,
				RunAsNonRoot: &runAsNonRoot,
				RunAsUser:    &runAsUser,
			},
			ServiceAccountName: monitors.Prometheus,
		},
	}, nil
}
