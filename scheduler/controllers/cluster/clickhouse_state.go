package cluster

import (
	"context"
	"fmt"
	"net/url"

	"github.com/opstrace/opstrace/go/pkg/constants"
	"github.com/opstrace/opstrace/scheduler/api/v1alpha1"
	"github.com/opstrace/opstrace/scheduler/controllers/cluster/clickhouse"
	"github.com/opstrace/opstrace/scheduler/controllers/cluster/clickhouseOperator"
	"github.com/opstrace/opstrace/scheduler/controllers/config"
	clickhousev1alpha1 "gitlab.com/gitlab-org/opstrace/opstrace/clickhouse-operator/api/v1alpha1"
	appsv1 "k8s.io/api/apps/v1"
	v1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	"sigs.k8s.io/controller-runtime/pkg/client"
)

type ClickHouseState struct {
	// Keep track of Credentials so we can pass them to our ClickHouse client
	Credentials *v1.Secret
	// Track deployment readiness
	OperatorDeployment *appsv1.Deployment
	// Track cluster deployment
	Cluster *clickhousev1alpha1.ClickHouse
}

func NewClickHouseState() *ClickHouseState {
	return &ClickHouseState{}
}

func (i *ClickHouseState) Read(ctx context.Context, cr *v1alpha1.Cluster, client client.Client) error {
	err := i.readCredentials(ctx, cr, client)
	if err != nil {
		return err
	}
	err = i.readDeployment(ctx, cr, client)
	if err != nil {
		return err
	}
	err = i.readCluster(ctx, cr, client)
	if err != nil {
		return err
	}
	return nil
}

// Get ClickHouse username/password for the scheduler
func (i *ClickHouseState) GetSchedulerCredentials() (*url.Userinfo, error) {
	e, err := i.GetEndpoints()
	if err != nil {
		return nil, err
	}
	// user is the same for http and native endpoints
	return e.Http.User, nil
}

// Get ClickHouse endpoints for the scheduler user
func (i *ClickHouseState) GetEndpoints() (*config.ClickHouseEndpoints, error) {
	if i.Credentials == nil || i.Credentials.Data[constants.ClickHouseCredentialsHTTPEndpointKey] == nil {
		return nil, fmt.Errorf("%s key not set in clickhouse scheduler credentials secret",
			constants.ClickHouseCredentialsHTTPEndpointKey)
	}
	http := string(i.Credentials.Data[constants.ClickHouseCredentialsHTTPEndpointKey])
	httpURL, err := url.Parse(http)
	if err != nil {
		return nil, err
	}
	if i.Credentials == nil || i.Credentials.Data[constants.ClickHouseCredentialsNativeEndpointKey] == nil {
		return nil, fmt.Errorf("%s key not set in clickhouse scheduler credentials secret",
			constants.ClickHouseCredentialsNativeEndpointKey)
	}
	native := string(i.Credentials.Data[constants.ClickHouseCredentialsNativeEndpointKey])
	nativeURL, err := url.Parse(native)
	if err != nil {
		return nil, err
	}

	return &config.ClickHouseEndpoints{
		Http:   *httpURL,
		Native: *nativeURL,
	}, nil
}

// Get ClickHouse cluster endpoint
func (i *ClickHouseState) isClusterReady() bool {
	if i.Cluster == nil {
		return false
	}
	return i.Cluster.Status.Status == clickhousev1alpha1.StatusCompleted
}

func (i *ClickHouseState) readCredentials(ctx context.Context, cr *v1alpha1.Cluster, client client.Client) error {
	currentState := &v1.Secret{}
	selector := clickhouse.CredentialsSelector(cr)
	err := client.Get(ctx, selector, currentState)
	if err != nil {
		if errors.IsNotFound(err) {
			return nil
		}
		return err
	}
	i.Credentials = currentState.DeepCopy()
	return nil
}

func (i *ClickHouseState) readDeployment(ctx context.Context, cr *v1alpha1.Cluster, client client.Client) error {
	currentState := &appsv1.Deployment{}
	selector := clickhouseOperator.DeploymentSelector(cr)
	err := client.Get(ctx, selector, currentState)
	if err != nil {
		if errors.IsNotFound(err) {
			return nil
		}
		return err
	}
	i.OperatorDeployment = currentState.DeepCopy()
	return nil
}

func (i *ClickHouseState) readCluster(ctx context.Context, cr *v1alpha1.Cluster, client client.Client) error {
	currentState := &clickhousev1alpha1.ClickHouse{}
	selector := clickhouse.ClickHouseSelector(cr)
	err := client.Get(ctx, selector, currentState)
	if err != nil {
		if errors.IsNotFound(err) {
			return nil
		}
		return err
	}
	i.Cluster = currentState.DeepCopy()
	return nil
}
