package prometheusOperator

import (
	"fmt"

	v1 "k8s.io/api/apps/v1"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/resource"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"sigs.k8s.io/controller-runtime/pkg/client"

	utils "github.com/opstrace/opstrace/go/pkg/common"
	"github.com/opstrace/opstrace/go/pkg/constants"
	"github.com/opstrace/opstrace/scheduler/api/v1alpha1"
)

const (
	MemoryRequest = "100Mi"
	CpuRequest    = "100m"
	MemoryLimit   = "200Mi"
	CpuLimit      = "200m"
)

var Replicas int32 = 1

func GetPrometheusOperatorDeploymentName() string {
	return constants.PrometheusOperatorName
}

func GetPrometheusOperatorDeploymentSelector() map[string]string {
	return map[string]string{
		"app": constants.PrometheusOperatorName,
	}
}

func getPrometheusOperatorResources() corev1.ResourceRequirements {
	return corev1.ResourceRequirements{
		Requests: corev1.ResourceList{
			corev1.ResourceMemory: resource.MustParse(MemoryRequest),
			corev1.ResourceCPU:    resource.MustParse(CpuRequest),
		},
		Limits: corev1.ResourceList{
			corev1.ResourceMemory: resource.MustParse(MemoryLimit),
			corev1.ResourceCPU:    resource.MustParse(CpuLimit),
		},
	}
}

func getPrometheusOperatorDeploymentStrategy() v1.DeploymentStrategy {
	return v1.DeploymentStrategy{
		Type: v1.RecreateDeploymentStrategyType,
	}
}

func getPrometheusOperatorDeploymentLabels() map[string]string {
	return map[string]string{
		"app": constants.PrometheusOperatorName,
	}
}

func getPrometheusOperatorDeploymentAnnotations(cr *v1alpha1.Cluster, existing map[string]string) map[string]string {
	return existing
}

func getPrometheusOperatorPodAnnotations(cr *v1alpha1.Cluster, existing map[string]string) map[string]string {
	return existing
}

func getPrometheusOperatorPodLabels() map[string]string {
	return map[string]string{
		"app": constants.PrometheusOperatorName,
	}
}

func getPrometheusOperatorContainers() []corev1.Container {
	var allowPrivilegeEscalation bool = false

	return []corev1.Container{{
		Name:  constants.PrometheusOperatorName,
		Image: constants.OpstraceImages().PrometheusOperatorImage,
		Args: []string{
			"--kubelet-service=kube-system/kubelet",
			fmt.Sprintf("--prometheus-config-reloader=%s", constants.OpstraceImages().PrometheusConfigReloaderImage),
		},
		Ports: []corev1.ContainerPort{
			{
				ContainerPort: 8080,
				Name:          "http",
			},
		},
		Resources:       getPrometheusOperatorResources(),
		ImagePullPolicy: corev1.PullIfNotPresent,
		SecurityContext: &corev1.SecurityContext{
			AllowPrivilegeEscalation: &allowPrivilegeEscalation,
		},
	}}
}

func getPrometheusOperatorDeploymentSpec(cr *v1alpha1.Cluster, current v1.DeploymentSpec) v1.DeploymentSpec {
	var user int64 = 65534
	var runAsNonRoot bool = true

	return v1.DeploymentSpec{
		Replicas: &Replicas,
		Selector: &metav1.LabelSelector{
			MatchLabels: GetPrometheusOperatorDeploymentSelector(),
		},
		Template: corev1.PodTemplateSpec{
			ObjectMeta: metav1.ObjectMeta{
				Name:        GetPrometheusOperatorDeploymentName(),
				Labels:      getPrometheusOperatorPodLabels(),
				Annotations: getPrometheusOperatorPodAnnotations(cr, current.Template.Annotations),
			},
			Spec: corev1.PodSpec{
				Containers:         getPrometheusOperatorContainers(),
				ServiceAccountName: GetPrometheusOperatorDeploymentName(),
				SecurityContext: &corev1.PodSecurityContext{
					RunAsUser:    &user,
					RunAsNonRoot: &runAsNonRoot,
				},
			},
		},
		Strategy: getPrometheusOperatorDeploymentStrategy(),
	}
}

func PrometheusOperatorDeployment(cr *v1alpha1.Cluster) *v1.Deployment {
	return &v1.Deployment{
		ObjectMeta: metav1.ObjectMeta{
			Name:      GetPrometheusOperatorDeploymentName(),
			Namespace: cr.Namespace(),
		},
	}
}

func PrometheusOperatorDeploymentSelector(cr *v1alpha1.Cluster) client.ObjectKey {
	return client.ObjectKey{
		Namespace: cr.Namespace(),
		Name:      GetPrometheusOperatorDeploymentName(),
	}
}

func PrometheusOperatorDeploymentMutator(cr *v1alpha1.Cluster, current *v1.Deployment) error {
	currentSpec := &current.Spec
	spec := getPrometheusOperatorDeploymentSpec(cr, current.Spec)
	// Apply default overrides
	if err := utils.PatchObject(
		currentSpec,
		&spec,
	); err != nil {
		return err
	}
	// Apply CR overrides
	if err := utils.PatchObject(
		currentSpec,
		cr.Spec.Overrides.PrometheusOperator.Components.Deployment.Spec,
	); err != nil {
		return err
	}
	current.Spec = *currentSpec
	current.Annotations = utils.MergeMap(
		getPrometheusOperatorDeploymentAnnotations(cr, current.Annotations),
		cr.Spec.Overrides.PrometheusOperator.Components.Deployment.Annotations,
	)
	current.Labels = utils.MergeMap(
		getPrometheusOperatorDeploymentLabels(),
		cr.Spec.Overrides.PrometheusOperator.Components.Deployment.Labels,
	)

	return nil
}
