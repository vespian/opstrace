package errortrackingapi

import (
	utils "github.com/opstrace/opstrace/go/pkg/common"
	"github.com/opstrace/opstrace/scheduler/api/v1alpha1"
	v1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/util/intstr"
	"sigs.k8s.io/controller-runtime/pkg/client"
)

func getServiceName() string {
	return GetErrorTrackingAPIDeploymentName()
}

func getServiceLabels() map[string]string {
	return GetErrorTrackingAPIDeploymentSelector()
}

func getServiceAnnotations(_ *v1alpha1.Cluster, existing map[string]string) map[string]string {
	if existing == nil {
		existing = make(map[string]string)
	}
	existing["cloud.google.com/neg"] = `{"ingress": false}`
	return existing
}

func getServicePorts() []v1.ServicePort {
	return []v1.ServicePort{
		{
			Name:       "http",
			Port:       8080,
			TargetPort: intstr.FromString("http"),
		},
		{
			Name:       "metrics",
			Port:       8081,
			TargetPort: intstr.FromString("metrics"),
		},
	}
}

func getServiceSpec() v1.ServiceSpec {
	return v1.ServiceSpec{
		Ports:    getServicePorts(),
		Selector: GetErrorTrackingAPIDeploymentSelector(),
		Type:     v1.ServiceTypeClusterIP,
	}
}

func Service(cr *v1alpha1.Cluster) *v1.Service {
	return &v1.Service{
		ObjectMeta: metav1.ObjectMeta{
			Name:        getServiceName(),
			Namespace:   cr.Namespace(),
			Labels:      getServiceLabels(),
			Annotations: getServiceAnnotations(cr, nil),
		},
		Spec: getServiceSpec(),
	}
}

func ServiceMutator(cr *v1alpha1.Cluster, current *v1.Service) error {
	current.Name = getServiceName()
	currentSpec := current.Spec.DeepCopy()
	spec := getServiceSpec()
	// Apply default overrides
	if err := utils.PatchObject(
		currentSpec,
		&spec,
	); err != nil {
		return err
	}
	// Apply CR overrides
	if err := utils.PatchObject(
		currentSpec,
		cr.Spec.Overrides.ErrorTrackingAPI.Components.Service.Spec,
	); err != nil {
		return err
	}
	current.Spec = *currentSpec
	current.Annotations = utils.MergeMap(
		getServiceAnnotations(cr, current.Annotations),
		cr.Spec.Overrides.ErrorTrackingAPI.Components.Service.Annotations,
	)
	current.Labels = utils.MergeMap(
		getServiceLabels(),
		cr.Spec.Overrides.ErrorTrackingAPI.Components.Service.Labels,
	)

	return nil
}

func ServiceSelector(cr *v1alpha1.Cluster) client.ObjectKey {
	return client.ObjectKey{
		Namespace: cr.Namespace(),
		Name:      getServiceName(),
	}
}
