package errortrackingapi

import (
	v1 "k8s.io/api/apps/v1"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/resource"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/util/intstr"
	"sigs.k8s.io/controller-runtime/pkg/client"

	"github.com/opstrace/opstrace/go/pkg/common"
	"github.com/opstrace/opstrace/go/pkg/constants"
	"github.com/opstrace/opstrace/scheduler/api/v1alpha1"
)

const (
	MemoryRequest = "256Mi"
	CpuRequest    = "100m"
	MemoryLimit   = "4Gi"
	CpuLimit      = "2"
)

var Replicas int32 = 3

func GetErrorTrackingAPIDeploymentName() string {
	return constants.ErrorTrackingAPIName
}

func GetErrorTrackingAPIDeploymentSelector() map[string]string {
	return map[string]string{
		"app": constants.ErrorTrackingAPIName,
	}
}

func getResources() corev1.ResourceRequirements {
	return corev1.ResourceRequirements{
		Requests: corev1.ResourceList{
			corev1.ResourceMemory: resource.MustParse(MemoryRequest),
			corev1.ResourceCPU:    resource.MustParse(CpuRequest),
		},
		Limits: corev1.ResourceList{
			corev1.ResourceMemory: resource.MustParse(MemoryLimit),
			corev1.ResourceCPU:    resource.MustParse(CpuLimit),
		},
	}
}

func getDeploymentStrategy() v1.DeploymentStrategy {
	maxUnavailable := intstr.FromString("25%")
	maxSurge := intstr.FromString("25%")

	return v1.DeploymentStrategy{
		Type: v1.RollingUpdateDeploymentStrategyType,
		RollingUpdate: &v1.RollingUpdateDeployment{
			MaxUnavailable: &maxUnavailable,
			MaxSurge:       &maxSurge,
		},
	}
}

func getDeploymentLabels() map[string]string {
	return map[string]string{
		"app": constants.ErrorTrackingAPIName,
	}
}

func getDeploymentAnnotations(cr *v1alpha1.Cluster, existing map[string]string) map[string]string {
	return existing
}

func getPodAnnotations(cr *v1alpha1.Cluster, existing map[string]string) map[string]string {
	return existing
}

func getPodLabels() map[string]string {
	return map[string]string{
		"app": constants.ErrorTrackingAPIName,
	}
}

func getContainerEnv(_ *v1alpha1.Cluster, clickhouseDSN string, apiBaseURL string) []corev1.EnvVar {
	return []corev1.EnvVar{
		{
			Name:  "API_BASE_URL",
			Value: apiBaseURL,
		},
		{
			Name:  "CLICKHOUSE_DSN",
			Value: clickhouseDSN,
		},
	}
}

// func getReadinessProbe() *corev1.Probe {
// 	return &corev1.Probe{
// 		ProbeHandler: corev1.ProbeHandler{
// 			HTTPGet: &corev1.HTTPGetAction{
// 				Port:   intstr.FromInt(8081),
// 				Path:   "/readyz",
// 				Scheme: corev1.URISchemeHTTP,
// 			},
// 		},
// 		InitialDelaySeconds: 5,
// 		PeriodSeconds:       3,
// 	}
// }

// func getLivenessProbe() *corev1.Probe {
// 	return &corev1.Probe{
// 		ProbeHandler: corev1.ProbeHandler{
// 			HTTPGet: &corev1.HTTPGetAction{
// 				Path:   "/healthz",
// 				Port:   intstr.FromInt(8081),
// 				Scheme: "HTTP",
// 			},
// 		},
// 		InitialDelaySeconds: 30,
// 		PeriodSeconds:       3,
// 		FailureThreshold:    10,
// 	}
// }

func getContainers(cr *v1alpha1.Cluster, clickhouseDSN string, apiBaseURL string) []corev1.Container {
	return []corev1.Container{{
		Name:  constants.ErrorTrackingAPIName,
		Image: constants.OpstraceImages().ErrorTrackingAPIImage,
		Env:   getContainerEnv(cr, clickhouseDSN, apiBaseURL),
		Ports: []corev1.ContainerPort{
			{
				Name:          "http",
				ContainerPort: 8080,
			},
			{
				Name:          "metrics",
				ContainerPort: 8081,
			},
		},
		Resources:       getResources(),
		ImagePullPolicy: corev1.PullIfNotPresent,
		// ReadinessProbe:  getReadinessProbe(),
		// LivenessProbe:   getLivenessProbe(),
	}}
}

func getDeploymentSpec(
	cr *v1alpha1.Cluster,
	current v1.DeploymentSpec,
	clickhouseDSN string,
	apiBaseURL string,
) v1.DeploymentSpec {
	var terminationGracePeriod int64 = 10

	return v1.DeploymentSpec{
		Replicas: &Replicas,
		Selector: &metav1.LabelSelector{
			MatchLabels: GetErrorTrackingAPIDeploymentSelector(),
		},
		Template: corev1.PodTemplateSpec{
			ObjectMeta: metav1.ObjectMeta{
				Name:        GetErrorTrackingAPIDeploymentName(),
				Labels:      getPodLabels(),
				Annotations: getPodAnnotations(cr, current.Template.Annotations),
			},
			Spec: corev1.PodSpec{
				Containers:                    getContainers(cr, clickhouseDSN, apiBaseURL),
				ServiceAccountName:            GetErrorTrackingAPIDeploymentName(),
				TerminationGracePeriodSeconds: &terminationGracePeriod,
				Affinity: common.WithPodAntiAffinity(metav1.LabelSelector{
					MatchLabels: GetErrorTrackingAPIDeploymentSelector(),
				}, nil),
			},
		},
		Strategy: getDeploymentStrategy(),
	}
}

func Deployment(cr *v1alpha1.Cluster) *v1.Deployment {
	return &v1.Deployment{
		ObjectMeta: metav1.ObjectMeta{
			Name:      GetErrorTrackingAPIDeploymentName(),
			Namespace: cr.Namespace(),
		},
	}
}

func DeploymentSelector(cr *v1alpha1.Cluster) client.ObjectKey {
	return client.ObjectKey{
		Namespace: cr.Namespace(),
		Name:      GetErrorTrackingAPIDeploymentName(),
	}
}

func DeploymentMutator(
	cr *v1alpha1.Cluster,
	current *v1.Deployment,
	clickhouseDSN string,
	apiBaseURL string,
) error {
	currentSpec := &current.Spec
	spec := getDeploymentSpec(cr, current.Spec, clickhouseDSN, apiBaseURL)
	// Apply default overrides
	if err := common.PatchObject(
		currentSpec,
		&spec,
	); err != nil {
		return err
	}
	// Apply CR overrides
	if err := common.PatchObject(
		currentSpec,
		cr.Spec.Overrides.ErrorTrackingAPI.Components.Deployment.Spec,
	); err != nil {
		return err
	}
	current.Spec = *currentSpec
	current.Annotations = common.MergeMap(
		getDeploymentAnnotations(cr, current.Annotations),
		cr.Spec.Overrides.ErrorTrackingAPI.Components.Deployment.Annotations,
	)
	current.Labels = common.MergeMap(
		getDeploymentLabels(),
		cr.Spec.Overrides.ErrorTrackingAPI.Components.Deployment.Labels,
	)

	return nil
}
