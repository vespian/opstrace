package jaegerOperator

import (
	certmanager "github.com/cert-manager/cert-manager/pkg/apis/certmanager/v1"
	"github.com/opstrace/opstrace/scheduler/api/v1alpha1"
	v1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"sigs.k8s.io/controller-runtime/pkg/client"
)

func Issuer(cr *v1alpha1.Cluster) *certmanager.Issuer {
	return &certmanager.Issuer{
		ObjectMeta: v1.ObjectMeta{
			Name:      "selfsigned-issuer",
			Namespace: cr.Namespace(),
		},
		Spec: certmanager.IssuerSpec{
			IssuerConfig: certmanager.IssuerConfig{
				SelfSigned: &certmanager.SelfSignedIssuer{},
			},
		},
	}
}

func IssuerMutator(cr *v1alpha1.Cluster, current *certmanager.Issuer) {
	issuer := Issuer(cr)
	current.Spec.SelfSigned = issuer.Spec.SelfSigned
}

func IssuerSelector(cr *v1alpha1.Cluster) client.ObjectKey {
	return client.ObjectKey{
		Namespace: cr.Namespace(),
		Name:      "selfsigned-issuer",
	}
}
