package clickhouse

import (
	utils "github.com/opstrace/opstrace/go/pkg/common"
	"github.com/opstrace/opstrace/go/pkg/constants"
	"github.com/opstrace/opstrace/scheduler/api/v1alpha1"
	monitoring "github.com/prometheus-operator/prometheus-operator/pkg/apis/monitoring/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"sigs.k8s.io/controller-runtime/pkg/client"
)

func getServiceMonitorName(cr *v1alpha1.Cluster) string {
	return constants.ClickHouseServiceMonitorName
}

func getServiceMonitorLabels() map[string]string {
	labels := map[string]string{
		"app": constants.ClickHouseServiceMonitorName,
	}
	// Configure the system-tenant prometheus to scrape this
	labels["tenant"] = "system"

	return labels
}

func getServiceMonitorAnnotations(cr *v1alpha1.Cluster) map[string]string {
	return map[string]string{}
}

func getServiceMonitorEndpoints() []monitoring.Endpoint {
	return []monitoring.Endpoint{
		{
			Port:     "metrics",
			Interval: "30s",
		},
	}
}

func getServiceMonitorSpec(cr *v1alpha1.Cluster) monitoring.ServiceMonitorSpec {
	return monitoring.ServiceMonitorSpec{
		Endpoints: getServiceMonitorEndpoints(),
		Selector: metav1.LabelSelector{
			MatchLabels: map[string]string{
				"app":  "clickhouse",
				"name": constants.ClickHouseClusterServiceName,
			},
		},
		JobLabel: constants.SelectorLabelName,
		NamespaceSelector: monitoring.NamespaceSelector{
			MatchNames: []string{cr.Namespace()},
		},
	}
}

func ServiceMonitor(cr *v1alpha1.Cluster) *monitoring.ServiceMonitor {
	return &monitoring.ServiceMonitor{
		ObjectMeta: metav1.ObjectMeta{
			Name:        getServiceMonitorName(cr),
			Namespace:   cr.Namespace(),
			Labels:      getServiceMonitorLabels(),
			Annotations: getServiceMonitorAnnotations(cr),
		},
		Spec: getServiceMonitorSpec(cr),
	}
}

func ServiceMonitorMutator(cr *v1alpha1.Cluster, current *monitoring.ServiceMonitor) error {
	current.Name = getServiceMonitorName(cr)
	currentSpec := current.Spec.DeepCopy()
	spec := getServiceMonitorSpec(cr)
	// Apply default overrides
	if err := utils.PatchObject(
		currentSpec,
		&spec,
	); err != nil {
		return err
	}
	crOverrides := cr.Spec.Overrides.ClickHouse.Components.ServiceMonitor.Spec
	// Apply CR overrides
	if err := utils.PatchObject(
		currentSpec,
		&crOverrides,
	); err != nil {
		return err
	}
	current.Spec = *currentSpec
	current.Annotations = utils.MergeMap(
		getServiceMonitorAnnotations(cr),
		cr.Spec.Overrides.ClickHouse.Components.ServiceMonitor.Annotations,
	)

	current.Labels = utils.MergeMap(
		getServiceMonitorLabels(),
		cr.Spec.Overrides.ClickHouse.Components.ServiceMonitor.Labels,
	)

	return nil
}

func ServiceMonitorSelector(cr *v1alpha1.Cluster) client.ObjectKey {
	return client.ObjectKey{
		Namespace: cr.Namespace(),
		Name:      getServiceMonitorName(cr),
	}
}
