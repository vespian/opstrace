package namespace

import (
	"github.com/opstrace/opstrace/go/pkg/constants"
	"github.com/opstrace/opstrace/scheduler/api/v1alpha1"
	"github.com/opstrace/opstrace/scheduler/controllers/config"
	tenantOperator "github.com/opstrace/opstrace/tenant-operator/api/v1alpha1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"sigs.k8s.io/controller-runtime/pkg/client"
)

func getTenantName(cr *v1alpha1.GitLabNamespace) string {
	return constants.TenantName
}

func getTenantLabels(cr *v1alpha1.GitLabNamespace) map[string]string {
	return map[string]string{}
}

func getTenantAnnotations(cr *v1alpha1.GitLabNamespace, existing map[string]string) map[string]string {
	return existing
}

func getTenantSpec() tenantOperator.TenantSpec {
	clusterSpec := config.Get().ClusterSpec().Spec
	domain := (&clusterSpec).GetHost()

	return tenantOperator.TenantSpec{
		Domain:           &domain,
		ImagePullSecrets: clusterSpec.ImagePullSecrets,
	}
}

func Tenant(cr *v1alpha1.GitLabNamespace) *tenantOperator.Tenant {
	return &tenantOperator.Tenant{
		ObjectMeta: metav1.ObjectMeta{
			Name:        getTenantName(cr),
			Namespace:   cr.Namespace(),
			Labels:      getTenantLabels(cr),
			Annotations: getTenantAnnotations(cr, nil),
		},
		Spec: getTenantSpec(),
	}
}

func TenantSelector(cr *v1alpha1.GitLabNamespace) client.ObjectKey {
	return client.ObjectKey{
		Name:      getTenantName(cr),
		Namespace: cr.Namespace(),
	}
}

func TenantMutator(cr *v1alpha1.GitLabNamespace, current *tenantOperator.Tenant) error {
	current.Labels = getTenantLabels(cr)
	current.Annotations = getTenantAnnotations(cr, current.Annotations)
	spec := getTenantSpec()

	current.Spec.Domain = spec.Domain
	current.Spec.ImagePullSecrets = spec.ImagePullSecrets

	return nil
}
