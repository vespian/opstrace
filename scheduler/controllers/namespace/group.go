package namespace

import (
	"fmt"

	"github.com/opstrace/opstrace/scheduler/api/v1alpha1"
	"github.com/opstrace/opstrace/scheduler/controllers/config"
	tenantOperator "github.com/opstrace/opstrace/tenant-operator/api/v1alpha1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"sigs.k8s.io/controller-runtime/pkg/client"
)

func getGroupName(cr *v1alpha1.GitLabNamespace) string {
	return fmt.Sprintf("%d", cr.Spec.ID)
}

func getGroupLabels(cr *v1alpha1.GitLabNamespace) map[string]string {
	return nil
}

func getGroupAnnotations(cr *v1alpha1.GitLabNamespace, existing map[string]string) map[string]string {
	return existing
}

func getGroupSpec(cr *v1alpha1.GitLabNamespace) tenantOperator.GroupSpec {
	return tenantOperator.GroupSpec{
		ID:               cr.Spec.ID,
		Name:             cr.Spec.Name,
		Path:             cr.Spec.Path,
		FullPath:         cr.Spec.FullPath,
		AvatarURL:        cr.Spec.AvatarURL,
		WebURL:           cr.Spec.WebURL,
		Domain:           config.Get().ClusterSpec().Spec.DNS.Domain,
		ImagePullSecrets: config.Get().ClusterSpec().Spec.ImagePullSecrets,
	}
}

func Group(cr *v1alpha1.GitLabNamespace) *tenantOperator.Group {
	return &tenantOperator.Group{
		ObjectMeta: metav1.ObjectMeta{
			Name:        getGroupName(cr),
			Namespace:   cr.Namespace(),
			Labels:      getGroupLabels(cr),
			Annotations: getGroupAnnotations(cr, nil),
		},
		Spec: getGroupSpec(cr),
	}
}

func GroupSelector(cr *v1alpha1.GitLabNamespace) client.ObjectKey {
	return client.ObjectKey{
		Name:      getGroupName(cr),
		Namespace: cr.Namespace(),
	}
}

func GroupMutator(cr *v1alpha1.GitLabNamespace, current *tenantOperator.Group) error {
	current.Labels = getGroupLabels(cr)
	current.Annotations = getGroupAnnotations(cr, current.Annotations)
	spec := getGroupSpec(cr)

	current.Spec.ID = spec.ID
	current.Spec.Name = spec.Name
	current.Spec.Path = spec.Path
	current.Spec.FullPath = spec.FullPath
	current.Spec.Description = spec.Description
	current.Spec.AvatarURL = spec.AvatarURL
	current.Spec.WebURL = spec.WebURL
	current.Spec.Domain = spec.Domain
	current.Spec.ImagePullSecrets = spec.ImagePullSecrets

	return nil
}
