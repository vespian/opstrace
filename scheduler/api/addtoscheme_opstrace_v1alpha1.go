package api

import (
	"github.com/opstrace/opstrace/scheduler/api/v1alpha1"
	tenantOperator "github.com/opstrace/opstrace/tenant-operator/api/v1alpha1"

	certmanager "github.com/cert-manager/cert-manager/pkg/apis/certmanager/v1"
	monitoring "github.com/prometheus-operator/prometheus-operator/pkg/apis/monitoring/v1"
	redis "github.com/spotahome/redis-operator/api/redisfailover/v1"
	clickhousev1alpha1 "gitlab.com/gitlab-org/opstrace/opstrace/clickhouse-operator/api/v1alpha1"
)

func init() {
	// Register the types with the Scheme so the components can map objects to GroupVersionKinds and back
	AddToSchemes = append(AddToSchemes,
		v1alpha1.SchemeBuilder.AddToScheme,
		clickhousev1alpha1.AddToScheme,
		monitoring.AddToScheme,
		certmanager.AddToScheme,
		tenantOperator.AddToScheme,
		redis.AddToScheme,
	)
}
