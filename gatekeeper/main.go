package main

import (
	"context"
	"crypto/tls"
	"encoding/gob"
	"flag"
	"fmt"
	"net/http"
	"net/url"
	"os"
	"os/signal"
	"path"
	"strconv"
	"syscall"
	"time"

	"github.com/gin-gonic/gin"
	log "github.com/sirupsen/logrus"
	"go.uber.org/zap/zapcore"
	"golang.org/x/oauth2"

	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/log/zap"

	"github.com/opstrace/opstrace/gatekeeper/gatekeeper"
	apis "github.com/opstrace/opstrace/scheduler/api"

	// Import all Kubernetes client auth plugins (e.g. Azure, GCP, OIDC, etc.)
	// to ensure that exec-entrypoint and run can make use of them.
	_ "k8s.io/client-go/plugin/pkg/client/auth"

	k8sruntime "k8s.io/apimachinery/pkg/runtime"
	utilruntime "k8s.io/apimachinery/pkg/util/runtime"
	clientgoscheme "k8s.io/client-go/kubernetes/scheme"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/healthz"

	opstracev1alpha1 "github.com/opstrace/opstrace/scheduler/api/v1alpha1"
)

var (
	scheme                      = k8sruntime.NewScheme()
	setupLog                    = ctrl.Log.WithName("setup")
	metricsAddr                 string
	probeAddr                   string
	port                        = getEnv("PORT", ":3001")
	redisAddr                   = getEnv("REDIS_ADDRESS", "127.0.0.1:6379")
	redisPassword               = getEnv("REDIS_PASSWORD", "")
	useSecureCookie             = getEnvBool("USE_SECURE_COOKIE")
	cookieName                  = getEnv("COOKIE_NAME", "opstrace.sid")
	cookieSecret                = getEnv("COOKIE_SECRET", "")
	oauthClientID               = getEnv("GITLAB_OAUTH_CLIENT_ID", "")
	oauthClientSecret           = getEnv("GITLAB_OAUTH_CLIENT_SECRET", "")
	serverDomain                = getEnv("DOMAIN", "http://localhost:3001")
	gitlabAddr                  = getEnv("GITLAB_INSTANCE_URL", "https://gitlab.com")
	gitlabInternalEndpointToken = getEnv("GITLAB_INTERNAL_ENDPOINT_TOKEN", "")
	groupAllowedAccess          = getEnv("GITLAB_GROUP_ALLOWED_ACCESS", "*")
	groupAllowedSystemAccess    = getEnv("GITLAB_GROUP_ALLOWED_SYSTEM_NAMESPACE_ACCESS", "")
	redisConnectionPoolSize     = getEnvInt("REDIS_CONNECTION_POOL_SIZE", 100)
)

func assignOpts() {
	flag.StringVar(&metricsAddr, "metrics-bind-address", ":8080", "The address the metric endpoint binds to.")
	flag.StringVar(&probeAddr, "health-probe-bind-address", ":8081", "The address the probe endpoint binds to.")

	opts := zap.Options{
		Development: true,
		// ISO8601TimeEncoder serializes a time.Time to an ISO8601-formatted
		// string with millisecond precision.
		TimeEncoder: zapcore.ISO8601TimeEncoder,
	}
	opts.BindFlags(flag.CommandLine)
	flag.Parse()

	ctrl.SetLogger(zap.New(zap.UseFlagOptions(&opts)))
}

func main() {
	utilruntime.Must(clientgoscheme.AddToScheme(scheme))
	utilruntime.Must(opstracev1alpha1.AddToScheme(scheme))

	assignOpts()

	if gitlabInternalEndpointToken == "" {
		log.Fatal("GITLAB_INTERNAL_ENDPOINT_TOKEN is a required parameter")
	}

	log.SetLevel(log.DebugLevel)
	// avoid [Error] gob: type not registered for interface: oauth2.Token
	gob.Register(oauth2.Token{})

	router := gin.Default()
	// For now, disable all proxies for X-Forwarded-For client IP purposes.
	// This affects context.ClientIP(), which we aren't using yet anyway.
	// See also https://pkg.go.dev/github.com/gin-gonic/gin#readme-don-t-trust-all-proxies
	router.SetTrustedProxies(nil)
	router.LoadHTMLGlob("templates/*.gohtml")

	// Parse gitlab address and set up the internal API endpoint
	internalEndpointAddr, err := url.Parse(gitlabAddr)
	if err != nil {
		log.WithError(err).Fatal("invalid gitlab addr")
	}
	internalEndpointAddr.Path = path.Join(internalEndpointAddr.Path, gatekeeper.GitlabInternalErrorTrackingEndpoint)

	namespace, err := GetWatchNamespace()
	if err != nil {
		log.Error(err, "failed to get namespace for watching/creating GitLabNamespace CRs")
		os.Exit(1)
	}
	// This client is a split client that reads objects from the cache and writes to the apiserver.
	// Keeping a cache that is automatically updated objects we care about (GitLabNamespace) reduces
	// load on the apiserver and provides realtime event updates
	k8sClient := StartControllerManager(namespace)

	router.Use(gatekeeper.Config(&gatekeeper.ConfigOptions{
		Port:                        port,
		RedisAddr:                   redisAddr,
		RedisPassword:               redisPassword,
		UseSecureCookie:             useSecureCookie,
		CookieName:                  cookieName,
		CookieSecret:                cookieSecret,
		OauthClientID:               oauthClientID,
		OauthClientSecret:           oauthClientSecret,
		ServerDomain:                serverDomain,
		GitlabAddr:                  gitlabAddr,
		GitlabInternalEndpointAddr:  internalEndpointAddr.String(),
		GitlabInternalEndpointToken: gitlabInternalEndpointToken,
		GroupAllowedAccess:          groupAllowedAccess,
		GroupAllowedSystemAccess:    groupAllowedSystemAccess,
		K8sClient:                   k8sClient,
		Namespace:                   namespace,
	}))

	// Setup our session middleware to use Redis for session persistence
	router.Use(gatekeeper.Session(&gatekeeper.SessionOptions{
		RedisAddr:       redisAddr,
		RedisPassword:   redisPassword,
		CookieName:      cookieName,
		CookieSecret:    cookieSecret,
		UseSecureCookie: useSecureCookie,
	}))
	// Configure OAuth2 and make it available to the auth controllers
	router.Use(gatekeeper.OAuth2(&gatekeeper.AuthOptions{
		ClientID:     oauthClientID,
		ClientSecret: oauthClientSecret,
		ServerDomain: serverDomain,
		GitlabAddr:   gitlabAddr,
	}))
	// Configure cache and postgres clients and make them available
	// to downstream handlers
	router.Use(gatekeeper.Cache(&gatekeeper.CacheOptions{
		RedisAddr:          redisAddr,
		RedisPassword:      redisPassword,
		ConnectionPoolSize: redisConnectionPoolSize,
	}))

	gatekeeper.SetRoutes(router)

	log.Infof("log level: %s", log.GetLevel())
	log.Infof("starting HTTP server on %s", port)

	if gin.Mode() != gin.DebugMode {
		serveWithGracefulShutdown(port, router)
	} else {
		router.Run(port)
	}
}

func StartControllerManager(namespace string) client.Client {
	mgr, err := ctrl.NewManager(ctrl.GetConfigOrDie(), ctrl.Options{
		Scheme:                 scheme,
		MetricsBindAddress:     metricsAddr,
		Port:                   9443,
		HealthProbeBindAddress: probeAddr,
		LeaderElection:         false,
		LeaderElectionID:       "",
	})
	if err != nil {
		setupLog.Error(err, "unable to create manager")
		os.Exit(1)
	}

	log.Info("Registering Scheme")

	if err := apis.AddToScheme(mgr.GetScheme()); err != nil {
		log.Error(err, "")
		os.Exit(1)
	}

	log.Info("Starting the controller manager")

	ctx := context.Background()
	ctx, cancel := context.WithCancel(ctx)
	if err = (&gatekeeper.NamespaceWatcher{
		Client:    mgr.GetClient(),
		Scheme:    mgr.GetScheme(),
		Log:       ctrl.Log.WithName("NamespaceWatcher"),
		Namespace: namespace,
		/* #nosec G402 */
		Transport: &http.Transport{
			TLSClientConfig: &tls.Config{
				InsecureSkipVerify: true,
			},
		},
		Context: ctx,
		Cancel:  cancel,
	}).SetupWithManager(mgr); err != nil {
		setupLog.Error(err, "unable to create controller", "controller", "Cluster")
		os.Exit(1)
	}

	// /healthz only checks if pod is up and running, and not if e.g. Redis
	// Sentinel is operational. For that we use /readyz endpoint in Gin server
	// itself.
	if err := mgr.AddHealthzCheck("healthz", healthz.Ping); err != nil {
		setupLog.Error(err, "unable to set up health check")
		os.Exit(1)
	}

	setupLog.Info("starting manager")
	go func() {
		if err := mgr.Start(ctrl.SetupSignalHandler()); err != nil {
			setupLog.Error(err, "problem running manager")
			os.Exit(1)
		}
	}()

	return mgr.GetClient()
}

func serveWithGracefulShutdown(port string, router http.Handler) {
	srv := &http.Server{
		Addr:    port,
		Handler: router,
	}

	go func() {
		// service connections
		if err := srv.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			log.Fatalf("failed to start server: %s\n", err)
		}
	}()

	// Wait for interrupt signal to gracefully shutdown the server
	quit := make(chan os.Signal, 1)
	signal.Notify(quit, syscall.SIGINT, syscall.SIGTERM)
	<-quit
	log.Info("shutting down gracefully")

	// Wait 10 seconds to flush existing connections
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)

	if err := srv.Shutdown(ctx); err != nil {
		cancel()
		log.Fatal("server shutdown error:", err)
	}
	defer cancel()

	<-ctx.Done()
	log.Info("server exiting")
}

func getEnv(key, fallback string) string {
	v := os.Getenv(key)
	if len(v) == 0 {
		return fallback
	}
	return v
}

func getEnvBool(key string) bool {
	v := os.Getenv(key)
	if len(v) == 0 {
		return false
	}
	bv, err := strconv.ParseBool(v)
	if err != nil {
		log.Fatalf("failed to convert env var %s into bool: %v", key, err)
	}
	return bv
}

func getEnvInt(key string, fallback int) int {
	v := os.Getenv(key)
	if len(v) == 0 {
		return fallback
	}
	iv, err := strconv.Atoi(v)
	if err != nil {
		log.Fatalf("failed to convert env var %s into bool: %v", key, err)
	}
	return iv
}

// GetWatchNamespace returns the Namespace the operator should be watching for changes.
func GetWatchNamespace() (string, error) {
	// WatchNamespaceEnvVar is the constant for env variable NAMESPACE
	// which specifies the Namespace to watch and create resource in.
	// An empty value means the operator is running with cluster scope.
	var watchNamespaceEnvVar = "NAMESPACE"

	ns, found := os.LookupEnv(watchNamespaceEnvVar)
	if !found {
		return "", fmt.Errorf("%s must be set", watchNamespaceEnvVar)
	}
	return ns, nil
}
