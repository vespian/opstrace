FROM golang:1.17 as base

# We'll use build targets so we can have a single dockerfile for dev and for release
# Dev build ###############################################################################################################
FROM base as dev
ENV GOPATH=/go
# Install the air binary so we get live code-reloading when we save files
RUN curl -sSfL https://raw.githubusercontent.com/cosmtrek/air/master/install.sh | sh -s -- -b $(go env GOPATH)/bin

WORKDIR /opt/go

CMD [ "air", "-c", ".air.gatekeeper.toml" ]

# Release build ###########################################################################################################
FROM base as build-env
ENV CGO_ENABLED=0
ENV GOOS=linux
ENV GOARCH=amd64
ENV GOPATH=/go

WORKDIR /workspace/go
# Copy common go packages
COPY go/go.mod ./
COPY go/go.sum ./
# cache deps before building and copying source so that we don't need to re-download as much
# and so that source changes don't invalidate our downloaded layer
RUN go mod download

COPY go/pkg/ pkg/

WORKDIR /workspace/tenant-operator
# Copy tenant operator
COPY tenant-operator/go.mod ./
COPY tenant-operator/go.sum ./
# cache deps before building and copying source so that we don't need to re-download as much
# and so that source changes don't invalidate our downloaded layer
RUN go mod download

COPY tenant-operator/api/ api/
COPY tenant-operator/controllers/ controllers/
COPY tenant-operator/version/ version/

WORKDIR /workspace/clickhouse-operator
# Copy clickhouse operator
COPY clickhouse-operator/go.mod ./
COPY clickhouse-operator/go.sum ./
# cache deps before building and copying source so that we don't need to re-download as much
# and so that source changes don't invalidate our downloaded layer
RUN go mod download

COPY clickhouse-operator/api/ api/
COPY clickhouse-operator/controllers/ controllers/

WORKDIR /workspace/scheduler
# Copy the Go Modules manifests
COPY scheduler/go.mod ./
COPY scheduler/go.sum ./
# cache deps before building and copying source so that we don't need to re-download as much
# and so that source changes don't invalidate our downloaded layer
RUN go mod download

# Copy the go source
COPY scheduler/api/ api/
COPY scheduler/controllers/ controllers/
COPY scheduler/version/ version/

WORKDIR /workspace/gatekeeper
# Copy common go packages
COPY gatekeeper/go.mod ./
COPY gatekeeper/go.sum ./
# cache deps before building and copying source so that we don't need to re-download as much
# and so that source changes don't invalidate our downloaded layer
RUN go mod download

COPY gatekeeper/gatekeeper/ gatekeeper/
COPY gatekeeper/templates/ templates/
COPY gatekeeper/main.go ./main.go

# Build
RUN go build -a -o server main.go

FROM scratch as release

COPY --from=build-env /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/
COPY --from=build-env /workspace/gatekeeper/server /
COPY --from=build-env /workspace/gatekeeper/templates/ /templates/

ENTRYPOINT ["/server"]
