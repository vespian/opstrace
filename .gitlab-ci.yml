include:
  - template: Security/SAST.gitlab-ci.yml
  - template: Security/SAST-IaC.latest.gitlab-ci.yml
  - template: Security/Dependency-Scanning.gitlab-ci.yml
  - template: Security/License-Scanning.gitlab-ci.yml
  # default CI template targetting merge requests, tags or main
  - template: "Workflows/MergeRequest-Pipelines.gitlab-ci.yml"
  # ClickHouse operator build jobs
  - local: clickhouse-operator/.gitlab-ci.yml

stages:
  - prepare
  - lint
  - build
  - test
  - images
  - image_scanning
  - e2e

variables:
  DOCKER_VERSION: "20.10.14"
  CHECKOUT_VERSION_STRING: ${CI_COMMIT_SHORT_SHA}-ci
  DOCKER_IMAGE_REGISTRY: $CI_REGISTRY_IMAGE
  # Bump the CI image version if the image changes or needs refreshing
  CI_IMAGE: $CI_REGISTRY_IMAGE/ci:0.0.7
  ## runner feature flag to try and speed up cache zipping - https://docs.gitlab.com/runner/configuration/feature-flags.html
  FF_USE_FASTZIP: "true"
  # compression levels set to fast (larger cache files)
  ARTIFACT_COMPRESSION_LEVEL: "fast"
  CACHE_COMPRESSION_LEVEL: "fast"
  GOMODCACHE: $CI_PROJECT_DIR/.go-mod-cache
  GO_VERSION: "1.17.10"

# parallel matrix for jobs to cover all our Go modules
.go-module-parallel:
  image: golang:${GO_VERSION}
  cache: &gomod-cache
    key:
      gomodules_${GOMOD_PATH}
      # would be great to use files based cache key based on var but you can't - https://gitlab.com/gitlab-org/gitlab/-/issues/118466
      # files:
      #   - $GOMOD_PATH/go.sum
    paths:
      - $GOMODCACHE
    policy: pull
  parallel:
    matrix:
      - GOMOD_PATH: go
      - GOMOD_PATH: tenant-operator
      - GOMOD_PATH: scheduler
      - GOMOD_PATH: gatekeeper
      - GOMOD_PATH: clickhouse-operator

# default jobs run with the custom CI image
default:
  image: $CI_IMAGE
  tags:
    # cost optimized non-docker runners by default
    - gitlab-org
  retry:
    max: 2
    when:
      - runner_system_failure
  interruptible: true

build CI image:
  stage: prepare
  extends:
    - .docker
  rules:
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
      changes:
        - containers/ci/opstrace-ci.Dockerfile
  script:
    # out Makefile is set up using bash, override this here (Alpine)
    - make BUILD_ARGS="--build-arg DOCKER_VERSION=$DOCKER_VERSION" rebuild-ci-container-image
    - make push-ci-container-image

# use for jobs that can start any time after the CI image is built
# optional - CI image only builds when changed
.needs-ci-image: &needs-ci-image
  - job: build CI image
    optional: true

# make buildinfo and docker-images and reuse these artifacts later
make constants:
  stage: prepare
  script:
    - make update-docker-images.json
  artifacts:
    paths:
      - go/pkg/constants/docker-images.json
  needs:
    - *needs-ci-image

gomod download:
  stage: prepare
  extends:
    - .go-module-parallel
  script:
    - cd $GOMOD_PATH && go mod download -x
  cache:
    policy: pull-push

lint docs:
  stage: lint
  image:
    name: "ghcr.io/igorshubovych/markdownlint-cli:latest"
    entrypoint: [""]
  rules:
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
      changes:
        - .gitlab/ci/**/*
        - .gitlab-ci.yml
        - Makefile
        - "**/Makefile"
        - ci/**/*
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
      changes:
        - .markdownlint.json
        - .gitattributes
        - docs/**/*
        - README.md
  cache: []
  script:
    # Override the entrypoint to be able to point at the markdown files.
    - /usr/local/bin/markdownlint "/builds/gitlab-org/opstrace/opstrace/docs/**/*.md" README.md

lint code:
  stage: lint
  image: golangci/golangci-lint:v1.45
  extends:
    - .go-module-parallel
  script:
    - make lint-code-${GOMOD_PATH}
  cache: []

# This stage verifies that all the changes to manifests were commited to the
# repo.
verify manifests sync:
  stage: lint
  extends:
    - .go-module-parallel
  script:
    - make regenerate-${GOMOD_PATH}
    - git status --porcelain
  cache: []

build:
  stage: build
  extends:
    - .go-module-parallel
  needs:
    # No point in building anything if we aren't building what is commited to
    # the repo.
    - verify manifests sync
  script:
    - make build-${GOMOD_PATH}

unit test:
  stage: test
  extends:
    # docker for tests that user testcontainers
    - .docker
    - .go-module-parallel
  # we don't need init to login to registry
  before_script: []
  script:
    - make unit-tests-${GOMOD_PATH}
  needs: [build]

images:
  stage: images
  extends:
    - .go-module-parallel
    # docker second, to use the docker specific image here
    - .docker
  script:
    - make docker-${GOMOD_PATH}
  needs: [unit test]
  cache: []

# generate child pipeline content based on controller image + docker-images.json
# TODO(joe): handle new scheduler image here once it has CI image builds
image scan config:
  stage: image_scanning
  needs:
    - job: make constants
    - job: images
      optional: true
  script:
    # TODO(joe): add the scheduler image to this too once available
    - >
      cat go/pkg/constants/docker-images.json |
      jq -r 'to_entries | .[] |
      "\(.key) image scan:
        extends: [.scanning_base]
        variables:
          DOCKER_IMAGE: \(.value)"' >> $PIPELINE_FILE
  artifacts:
    paths:
      - $PIPELINE_FILE
  variables:
    PIPELINE_FILE: .gitlab/ci/container_scanning.yml

image scan pipeline:
  stage: image_scanning
  needs:
    - image scan config
  trigger:
    include:
      - artifact: .gitlab/ci/container_scanning.yml
        job: image scan config

.e2e test GCP:
  extends:
    - .e2e-test
  variables:
    OPSTRACE_CLOUD_PROVIDER: "gcp"

# gcp and aws e2e tests extend this definition
.e2e-test:
  stage: e2e
  image: $CI_IMAGE
  extends:
    - .docker
  before_script:
    # cluster name can be 23 characters long, use them all if possible
    - export OPSTRACE_CLUSTER_NAME="${CI_COMMIT_REF_SLUG:0:10}-gl-${CI_COMMIT_SHORT_SHA}"
    # remove -- (double dash) from cluster name name; fixes https://gitlab.com/gitlab-org/opstrace/opstrace/-/issues/1661
    - export OPSTRACE_CLUSTER_NAME=${OPSTRACE_CLUSTER_NAME//--}
    - mkdir -p $OPSTRACE_ARTIFACT_DIR
  script:
    # secrets required to set up cluster
    - make fetch-secrets
    - make deploy-testremote-teardown
  variables:
    # artifacts dir for sizeable logs
    OPSTRACE_ARTIFACT_DIR: $CI_PROJECT_DIR/artifacts
    # Reenable when we commission a new Opstrace collection instance that we can use
    # to collect CI data.
    CI_DATA_COLLECTION: "disabled"
    # Set in CI/CD project settings
    TF_VAR_registry_auth_token: $TF_VAR_registry_auth_token
  # artifacts from complex scripts stored here, e.g. large error logs
  artifacts:
    name: job artifacts
    paths:
      - $OPSTRACE_ARTIFACT_DIR/
    # not public, just in case these artifacts expose secrets
    public: false
    when: always
  needs:
    - job: make constants
    - job: images
      optional: true
  # Don't allow e2e jobs to be interrupted because we need to teardown clusters.
  interruptible: false

# docker defaults
.docker:
  image: docker:${DOCKER_VERSION}-git
  services:
    - docker:${DOCKER_VERSION}-dind
  variables:
    DOCKER_DRIVER: overlay2
    DOCKER_HOST: tcp://docker:2376
    DOCKER_TLS_VERIFY: 1
    DOCKER_TLS_CERTDIR: "/certs"
    DOCKER_CERT_PATH: "/certs/client"
  tags:
    - gitlab-org-docker
  before_script:
    # Note(joe): alpine docker image doesn't have Make, we might consider baking a custom base image if this is slow,
    # also perl-utils required for shasum, used to generate image tags from dir contents.
    - apk add --no-cache make perl-utils
    - echo "${CI_REGISTRY_PASSWORD}" | docker login --username "${CI_REGISTRY_USER}" --password-stdin "${CI_REGISTRY}"

# overrides for security scanning - included at the top

# make the tools work on merge request pipelines as they check for CI_COMMIT_BRANCH
.security_tool: &security_tool
  rules:
    - if: $CI_PIPELINE_SOURCE == "merge_request_event" || $CI_COMMIT_BRANCH
  needs: []

gemnasium-dependency_scanning:
  <<: *security_tool

kics-iac-sast:
  <<: *security_tool

kubesec-sast:
  <<: *security_tool

gosec-sast:
  <<: *security_tool

semgrep-sast:
  <<: *security_tool

license_scanning:
  <<: *security_tool
  needs:
    - job: gomod download
      optional: true
  variables:
    LICENSE_FINDER_CLI_OPTS: "--enabled-package-managers=gomodules --aggregate_paths= go/ tenant-operator/ clickhouse-operator/ scheduler/ gatekeeper/"
  cache:
    - *gomod-cache
